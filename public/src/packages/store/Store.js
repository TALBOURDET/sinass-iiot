import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    alertFooter:{
        msg: ''
    },
    services:{},
    level:{
      isUser:null,
      isGroupAdmin:null,
      isAdmin:null,
      isAdminOrGroupAdmin:null
    },
    accessLevel:0,
    loader:false,
    auth:{
        isAuthenticated:false,
        token:null,
        expiration:null,
        username:null,
        accessLevel:null
    }
};
const mutations = {
  setLoader:(state, loaderState) =>{
    state.loader=loaderState
  },
  setAlertFooter: (state, msg) =>{
    state.alertFooter.msg = msg
//     if (msg === 'ConnectionError'){
//       state.alertFooter.msg = 'Action Impossible';
//     } else{
//       state.alertFooter.msg = ''
//     } 
  },
  resetAuthenticated: (state) => {
      state.auth.isAuthenticated = false
      state.auth.token = null
      state.auth.expiration = null
      state.auth.username = null
  },
  setServiceState: (state, payload) => {
      state.services=payload
  },
  setAuthenticated: (state, auth) => {
      state.auth.isAuthenticated = auth.isAuthenticated
      state.auth.token = auth.token
      state.auth.expiration = auth.expiration
      state.auth.username = auth.username
  },
  setAuthenticatedAccessLevel: (state,payload) => {
      state.auth.accessLevel = payload.accessLevel
      state.level.isUser = state.auth.accessLevel==1
      state.level.isGroupAdmin = state.auth.accessLevel==15
      state.level.isAdmin = state.auth.accessLevel==20
      state.level.isAdminOrGroupAdmin = (state.auth.accessLevel==20 || state.auth.accessLevel==15)
  }
  
}
const actions = {
  setToken(context, payload) {
      context.commit('setAuthenticated', payload)
      Vue.http.headers.common['Authorization'] = 'Bearer ' + payload.token
      if (payload.token && payload.expiration) {
        localStorage.setItem('username', payload.username)
        localStorage.setItem('token', payload.token)
        localStorage.setItem('expiration', payload.expiration)
        localStorage.setItem('isAuthenticated', payload.isAuthenticated)
      }
      
  },
  setAccessLevel(context, payload){
    context.commit('setAuthenticatedAccessLevel', payload)
    localStorage.setItem('accessLevel', payload.accessLevel) // reseted in destroyToken()
  },
  destroyToken() {
    localStorage.removeItem('username')
    localStorage.removeItem('isAuthenticated')
    localStorage.removeItem('token')
    localStorage.removeItem('expiration')
    localStorage.removeItem('accessLevel')
  },
  isAuthenticated(context){
    let auth = {}
    auth.isAuthenticated = localStorage.getItem('isAuthenticated')
    auth.username = localStorage.getItem('username')
    auth.token = localStorage.getItem('token')
    auth.expiration = localStorage.getItem('expiration')
    
    if (! auth.token || ! auth.expiration || ! auth.username || ! auth.isAuthenticated) { 
      context.commit('resetAuthenticated')
    }
    if (Date.now() > parseInt(auth.expiration)){
      context.dispatch('destroyToken')
      context.commit('resetAuthenticated')
    } else {
      context.dispatch('setToken',auth) 
      Vue.http.headers.common['Authorization'] = 'Bearer ' + auth.token
    }      
  },
  downloadFile(context, payload){
    let a = document.createElement("a");
    document.body.appendChild(a);
    a.style = "display: none";
    let data = payload.content
    let blob = new Blob([data], {type: "octet/stream"})
    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = payload.name;
    a.click();
    window.URL.revokeObjectURL(url);
  }
}
const getters = {
  isUser :state => state.level.isUser,
  isGroupAdmin :state => state.level.isAGroupAdmin,
  isAdmin :state => state.level.isAdmin,
  isAdminOrGroupAdmin :state => state.level.isAdminOrGroupAdmin,
  getServices: state=> state.services,
  getAlertFooter: state => state.alertFooter.msg,
  getAuth: state => state.auth,
  getLoader: state => state.loader
}
export default new Vuex.Store({  
  state:state,
  mutations:mutations,
  actions:actions,
  getters:getters,
  strict: true
})