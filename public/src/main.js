import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'
import Vuex from 'vuex'
import VueFormGenerator from "vue-form-generator"


import appUtils from './assets/js/utils'
import store from './packages/store/Store'
import _ from 'lodash'
import $ from 'jquery'

global.jQuery = $





require('./assets/js/bootstrap.js')
require('./assets/css/bootstrap.css')
require('./assets/css/app.css')



Vue.use(VueRouter)
Vue.use(VueResource)
Vue.use(Vuex)
Vue.use(VueFormGenerator)

const router = new VueRouter({
  mode: 'history',
  routes:[{
    path: '/login',
    component: require('./components/login.vue'),
    name:'login',
    meta:{
        forVisitors:true
    }
  },
  {
    path: '/equipments',
    component: require('./components/equipments.vue'),
    name:'equipments',
    meta:{
        forAuth:true
    }
  },
  {
    path: '/alarms',
    component: require('./components/alarms.vue'),
    name:'alarms',
    meta:{
        forAuth:true
    }
  },
  {
    path: '/dashboards/:id',
    component: require('./components/dashboards.vue'),
    name:'dashboards',
    props: true,
    meta:{
        forAuth:true
    }
  },
  {
    path: '/moveDashboards/:id',
    component: require('./components/moveDashboards.vue'),
    name:'moveDashboards',
    props: true,
    meta:{
        forAuth:true
    }
  },        
  {
    path: '/nodeRed',
    component: require('./components/nodeRed.vue'),
    name:'nodeRed',
    meta:{
        forAuth:true
    }
  },
  {
    path: '/menuConf',
    component: require('./components/menuConf.vue'),
    name:'menuConf',
    meta:{
        forAuth:true
    }
  },
  {
    path: '/grafana',
    component: require('./components/grafana.vue'),
    name:'grafana',
    meta:{
        forAuth:true
    }
  },
  {
    path: '/url/:id',
    component: require('./components/url.vue'),
    name:'url',
    props: true,
    meta:{
        forAuth:true
    }
  },
  {
    path: '/equipment/:id',
    component: require('./components/equipment.vue'),
    name:'equipment',
    props: true,
    meta:{
        forAuth:true
    }
  },
  {
    path: '/account',
    component: require('./components/users/account.vue'),
    name:'account',
    props: true,
    meta:{
        forAuth:true
    }
  },
  {
    path: '/users',
    component: require('./components/users/users.vue'),
    name:'users',
    meta:{
        forAuth:true
    },
    children:[{
      path: '/usersUsers',
      component: require('./components/users/usersUsers.vue'),
      name:'usersUsers',
      meta:{
        forAuth:true
    }
    },
    {
      path: '/usersGroups',
      component: require('./components/users/usersGroups.vue'),
      name:'usersGroups',
      meta:{
        forAuth:true
      }
    }]
  },
  {
    path: '/systeme',
    component: require('./components/systeme/systeme.vue'),
    name:'systeme',
    meta:{
        forAuth:true
    },
    children:[{
      path: '/systemeServices',
      component: require('./components/systeme/services.vue'),
      name:'systemeServices',
      meta:{
        forAuth:true
      }
    },
    {
      path: '/systemeVpnInit',
      component: require('./components/systeme/vpnInit.vue'),
      name:'systemeVpnInit',
      meta:{
        forAuth:true
      }
    },
		{
      path: '/systemeFirewall',
      component: require('./components/systeme/firewall.vue'),
      name:'systemeFirewall',
      meta:{
        forAuth:true
      }
    },
    {
      path: '/systemeAPropos',
      component: require('./components/systeme/aPropos.vue'),
      name:'systemeApropos',
      meta:{
        forAuth:true
      }
    },
    {
      path: '/systemeSupport',
      component: require('./components/systeme/support.vue'),
      name:'systemeSupport',
      meta:{
        forAuth:true
      }
    }]
  },
  {
    path: '/map',
    component: require('./components/map.vue'),
    name:'map',
    meta:{
        forAuth:true
    }
  },
  {
    path: '*',
    redirect: '/login'
  }]
})

router.beforeEach(
    (to, from, next) => {
        store.dispatch('isAuthenticated')
        if(to.matched.some(record => record.meta.forVisitors)){
            if(store.getters.getAuth.isAuthenticated){
                next({
                  path: 'map'
                })
            } else next()
        } else if(to.matched.some(record => record.meta.forAuth)){
            if(!store.getters.getAuth.isAuthenticated){
                next({
                  path: '/login'
                })
            } else next()
        }else next()
    }
)



const app = new Vue({

  el: '#app',
  router:router,
  store,
  render: h => h(require('./App.vue'))


})
