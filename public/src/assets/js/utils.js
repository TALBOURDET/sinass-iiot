/** 
* This library regroup methods for simples operations
*
* @author TALBOURDET Julien
*/
class utils {
  
    constructor() {
      window.appUtils = this
    }
  
  
    /**
    * Find an object in an array of objects
    *
    * @param {array} ObjArray - object array
    * @param {int} idObj      _ Id of the found object
    */
   findObjInArray(ObjArray, idObj) {
      let index = 0
      while (index !== ObjArray.length) {

        if (ObjArray[index].id === idObj) {
          return this.clone(ObjArray[index])
          break
        }
        index++
      }
    }
  
  
    /**
    * duplicate object
    *
    * @param {object} obj - object to duplicate
    **/
    clone(obj) {
      let copy = null
      try {
        copy = JSON.parse(JSON.stringify(obj));
      } catch (ex) {
        alert("Fonction non supporté par votre navigateur")
      }
      return copy
    }
		
		/**
    * Verify input as password or email with comfirmation
    *
    * @param {string} input1 - input field
		* @param {string} input2 - input confirmation field
    **/
    inputConfirmation(input1, input2) {
      if ((input1!=null && input2!=null) && (input1===input2)){
				return true
			} else{
				return false
			}
    }
  }


const appUtils = new utils()
export default appUtils


