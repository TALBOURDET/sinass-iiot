<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h4><i>Bonjour: </i>{{ $user->firstName }} {{ $user->lastName }}</h4>
        </br>
        <p>
          Voici vos identifiants de connexion.<br>
          Vous pouvez maintenant vous connecter sur <a href="{{ config('app.url') }}">www.sinass.fr</a></br>
        </p>
        </br>
        
          <p><i><b>Identifiant: </b></i>{{ $user->email }}</p></br>
          <p><i><b>Mot de passe: </b></i>{{ $user->passwordMail }}</p></br>
          <p>Pour des raisons de sécurité, nous vous recommandons de le personnaliser.</p>
        
        
    </body>
</html>
