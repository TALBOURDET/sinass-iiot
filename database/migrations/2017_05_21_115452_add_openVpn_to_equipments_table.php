<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOpenVPNToEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipments', function($table) {
            $table->string('networkMask')->nullable();
            $table->string('caDir')->nullable();
            $table->string('certDir')->nullable();
            $table->string('keyDir')->nullable();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipments', function($table) {
            $table->dropColumn('networkMask');
            $table->dropColumn('caDir');
            $table->dropColumn('certDir');
            $table->dropColumn('keyDir');
        });
    }
}
