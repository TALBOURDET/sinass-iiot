<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDashboard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('dashboards', function (Blueprint $table) {    
        $table->increments('id')->unique();
        $table->string('name')->unique();
        $table->integer('idGroup')->nullable();
        $table->integer('idEquipment')->nullable();
        $table->string('type')->nullable();
        $table->string('url')->nullable();
        $table->string('iframe')->nullable();
        $table->longText('data')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dashboards');
    }
}
