<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVpnclient extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vpn_clients', function (Blueprint $table) {
            $table->increments('id')->unique();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->integer('equipment_id')->unsigned()->index()->nullable();
            $table->string('commonName')->unique()->index();
            $table->string('ip')->nullable();
            $table->string('networkMask')->nullable();
            $table->string('caDir')->nullable();
            $table->string('certDir')->nullable();
            $table->string('keyDir')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vpn_clients'); 
    }
}
