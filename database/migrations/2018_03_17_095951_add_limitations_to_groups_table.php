<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLimitationsToGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('groups', function($table) {
          $table->integer('nbUsers')->nullable();
          $table->integer('nbEquipments')->nullable();
          $table->integer('nbSimultConnections')->nullable();
          $table->integer('nbActualSimultConnections')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function($table) {
          $table->dropColumn('nbUsers');
          $table->dropColumn('nbEquipments');
          $table->dropColumn('nbSimultConnections');
          $table->dropColumn('nbActualSimultConnections');
      });
    }
}
