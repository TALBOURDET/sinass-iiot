<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLocalisationToEquipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipments', function($table) {
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->boolean('showOnMap');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipments', function($table) {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('showOnMap');
        });
    }
}
