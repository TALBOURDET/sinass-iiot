<?php

use Illuminate\Database\Seeder;

class VpnServerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vpn_servers')->insert([
            'id' => 1,
            'ip' => '',
            'networkMask' => '',
            'clientToClient' => true,
            'country' => '',
            'province' => '',
            'city' => '',
            'mail' => '',
        ]);
    }
}
