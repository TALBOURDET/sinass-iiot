<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'login' => 'admin',
            'firstName' => 'admin',
            'lastName' => 'admin',
            'idGroup' => 1,
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'accessLevel' => 20,
        ]);
        DB::table('groups')->insert([
            'name' => 'admin',
        ]);
    }
}
