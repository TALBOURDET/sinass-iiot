#!/usr/bin/python3



#sudo apt install python3-pip
# pip3 install sh
#sudo apt-get install python3-mysql.connector
from datetime import datetime
import os
import time
import sh
import re
import mysql.connector


#
# Retourne la liste des clients connectés
#
#
def getConnectedClients():
  out=sh.OpenVpn("-cStates")
  out=out.split('\n')

  line = 0
  numConnectedClient = 0
  connectedClients=[]
  startList = False
  while line < len(out):
      if out[line-1]=="Virtual Address,Common Name,Real Address,Last Ref":
        startList=True
      if out[line]=="GLOBAL STATS":
        startList=False
        break
      if startList==True:
        actualLine=out[line].split(',')
        connectedClients.append({})
        connectedClients[numConnectedClient]["ip"]=actualLine[0]
        connectedClients[numConnectedClient]["commonName"]=actualLine[1]
        connectedClients[numConnectedClient]["date"]=datetime.strptime(actualLine[3], '%a %b %d %H:%M:%S %Y')
        numConnectedClient +=1
      line += 1
  def fctSortDict(value):
    return value['date']
  connectedClients.sort(key=fctSortDict)
  return connectedClients
#
# Retourne la liste des utilisateur connectés
#
#
def getConnectedUsersClients(connectedClients):
  global dbMySql
  cursorVpn_clients = dbMySql.cursor()
  cursorAccessLevels = dbMySql.cursor()
  connectedUsersClients=[]
  line = 0
  numRow=0
  while line < len(connectedClients):
    cursorVpn_clients.execute("""SELECT commonName, user_id FROM vpn_clients WHERE commonName=%s""" , (connectedClients[line]["commonName"],))
    rows = cursorVpn_clients.fetchall()
    for row in rows:
      cursorAccessLevels.execute("""SELECT accessLevel FROM users WHERE id=%s""" , (row[1],))
      usr = cursorAccessLevels.fetchall()
      connectedUsersClients.append({})
      connectedUsersClients[numRow]["ip"]=connectedClients[line]["ip"]
      connectedUsersClients[numRow]["commonName"]=row[0]
      connectedUsersClients[numRow]["user_id"]=row[1]
      connectedUsersClients[numRow]["user_accessLevel"]=usr[0][0]
      numRow += 1
    line += 1
  return connectedUsersClients
#
# Retourne les limitations de chaque groupe utilisateur
#
#
def getGroupslimitations():
	groupsLimitations = {}
	global dbMySql
	cursor = dbMySql.cursor()
	cursor.execute("""SELECT groups.id, groups.name, groups.nbSimultConnections FROM groups""" )
	rows = cursor.fetchall()
	for row in rows:
		groupsLimitations[row[0]]={}
		groupsLimitations[row[0]]['nbSimultConnectionsLimit']=row[2]
		groupsLimitations[row[0]]['id']=row[0]
		groupsLimitations[row[0]]['name']=row[1]
		groupsLimitations[row[0]]['nbSimultConnections']=0

	return groupsLimitations
#
# Actualise la charge de chaque groupe dans la base de données
#
#
def setDbGroupLoad(groupsLimitations):
	global dbMySql
	cursor = dbMySql.cursor()
	for groupsLimitation in groupsLimitations.values():
		cursor.execute("""UPDATE groups SET nbActualSimultConnections=%s WHERE id=%s""" , (groupsLimitation['nbSimultConnections'],groupsLimitation['id'],))	
#
# Retourne la liste des règles de redirections
#
#
def getEquipmentsConnectedUsers(connectedUsersClients):
	groupsLimitations = getGroupslimitations()
	global SinassParameters
	global dbMySql
	userClientlist={}
	fireWallRulesList=[]
	cursor = dbMySql.cursor()
	numUserClient=0
	numRow=0
	connectedUsersNumber=0
	for UserClient in connectedUsersClients:
		
		if UserClient["user_accessLevel"]==20:
			cursor.execute("""SELECT groups.id, groups.name, users.email FROM groups INNER JOIN users WHERE users.id=%s""" , (UserClient["user_id"],))
			rows = cursor.fetchall()
			##### Verification s'il n'y à pas trop d'utilisateurs conncetés dans l'application
			if (connectedUsersNumber+1 > int(SinassParameters['SINASS_NB_SIMULT_CONNECTIONS'])):
				authorizeAccess=False
			else:
				try:
					userClientlist[UserClient["user_id"]]=userClientlist[UserClient["user_id"]]+1
				except:
					userClientlist[UserClient["user_id"]]=0
					userClientlist[UserClient["user_id"]]=userClientlist[UserClient["user_id"]]+1
				connectedUsersNumber=connectedUsersNumber+1
				authorizeAccess=True
		else:
			cursor.execute("""SELECT groups.id, groups.name, users.email FROM groups INNER JOIN users ON groups.id = users.idGroup WHERE users.id=%s""" , (UserClient["user_id"],))
			rows = cursor.fetchall()
			groupsLimitations[rows[0][0]]['nbSimultConnections']=groupsLimitations[rows[0][0]]['nbSimultConnections']+1
			##### Verification s'il n'y à pas trop d'utilisateurs conncetés dans le groupe
			if ((groupsLimitations[rows[0][0]]['nbSimultConnections'] > groupsLimitations[rows[0][0]]['nbSimultConnectionsLimit']) or (connectedUsersNumber+1 > int(SinassParameters['SINASS_NB_SIMULT_CONNECTIONS']))):
				authorizeAccess=False
			else:
				try:
					userClientlist[UserClient["user_id"]]=userClientlist[UserClient["user_id"]]+1
				except:
					userClientlist[UserClient["user_id"]]=0
					userClientlist[UserClient["user_id"]]=userClientlist[UserClient["user_id"]]+1
				connectedUsersNumber=connectedUsersNumber+1
				authorizeAccess=True
		# Création de la règle si validé précédement
		if (authorizeAccess==True):
			for row in rows:
				emailUser=row[2]
				ipUser=UserClient["ip"]
				cursor.execute("""SELECT ip, name FROM equipments WHERE idGroup=%s""" , (row[0],))
				rowsEquipement = cursor.fetchall()
				for rowEquipement in rowsEquipement:
					if rowEquipement[1]!="":
						fireWallRulesList.append({})
						fireWallRulesList[numRow]["emailUser"]=emailUser
						fireWallRulesList[numRow]["ipUser"]=ipUser
						fireWallRulesList[numRow]["nameEquipment"]=rowEquipement[1]
						fireWallRulesList[numRow]["ipEquipment"]=rowEquipement[0]
						numRow += 1
	setDbGroupLoad(groupsLimitations)
	setDbUserConnection(userClientlist)
	return fireWallRulesList
#
# Actualise le nombre de connexions de chaque client
#
#
def setDbUserConnection(userClientlist):
	global dbMySql
	cursor = dbMySql.cursor()
	cursor.execute("""UPDATE users SET nbConnections=0""")	
	for id,nb in userClientlist.items():
		cursor.execute("""UPDATE users SET nbConnections=%s WHERE id=%s""" , (nb,id,))	
#
# Retourne la liste de règles de redirections déja établies
#
#
def getForwardingRules():
  out=sh.iptables("-L", "--line-numbers")
  out=out.split('\n')
  line=0
  numForwardingRules=0
  actualForwardingRules=[]
  startList=False
  while line < len(out):
      if out[line-2]=="Chain FORWARD (policy ACCEPT)": #Origine -1
        startList=True
      if startList==True and out[line+3]=="Chain OUTPUT (policy ACCEPT)": #Origine +2
        startList=False
        return actualForwardingRules
      if startList==True:
        memActualLine=re.sub(r'\s+',' ',out[line+1])
        actualLine=memActualLine.split(' ')  
        actualForwardingRules.append({})
        actualForwardingRules[numForwardingRules]["ipUser"]=actualLine[4]
        actualForwardingRules[numForwardingRules]["ipEquipment"]=actualLine[5]
        numForwardingRules +=1  
      line += 1
#
# Supprime les règles qui n'ont plus lieu d'êtres
#
#
def deleteForwardingRules(actualForwardingRules, futureForwardingRules):
  numActualRule=0
  while numActualRule < len(actualForwardingRules):
    numFutureRule=0
    delete=True
    while numFutureRule < len(futureForwardingRules):
      if (actualForwardingRules[numActualRule]['ipUser']==futureForwardingRules[numFutureRule]['ipUser']) and (actualForwardingRules[numActualRule]['ipEquipment']==futureForwardingRules[numFutureRule]['ipEquipment']):
        delete=False
      numFutureRule += 1
    if (delete==True):
      try:
        out=sh.iptables("--delete=FORWARD", "--in-interface=tun0","--jump=DROP")
      except:
        print ("Erreur suppression des règles de bases")
      try:
        out=sh.iptables("--delete=FORWARD", "--match=conntrack", "--ctstate=RELATED,ESTABLISHED", "--in-interface=tun0", "--jump=ACCEPT")
      except:
        print ("Erreur suppression des règles de bases")
        
        
      try:
        out=sh.iptables("--delete=FORWARD", "--source="+ actualForwardingRules[numActualRule]['ipUser'], "--destination="+ actualForwardingRules[numActualRule]['ipEquipment'], "--in-interface=tun0", "--jump=ACCEPT")
      except:
        print ("Erreur suppression de règle Utilisateur")
      
      
      try:
        out=sh.iptables("--append=FORWARD", "--jump=DROP", "--in-interface=tun0")
      except:
        print ("Erreur suppression des règles de bases")
      try:
        out=sh.iptables("--insert=FORWARD", "--match=conntrack", "--ctstate=RELATED,ESTABLISHED", "--jump=ACCEPT", "--in-interface=tun0")
      except:
        print ("Erreur suppression des règles de bases")
        
    numActualRule += 1
#
# Ajoute les nouvelles règles
#
#
def addForwardingRules(actualForwardingRules, futureForwardingRules):
  
  numFutureRule=0
  while numFutureRule < len(futureForwardingRules):
    numActualRule=0
    add=True
    while numActualRule < len(actualForwardingRules):
      if (actualForwardingRules[numActualRule]['ipUser']==futureForwardingRules[numFutureRule]['ipUser']) and (actualForwardingRules[numActualRule]['ipEquipment']==futureForwardingRules[numFutureRule]['ipEquipment']):
        add=False
      numActualRule += 1
    if (add==True):
      try:
        out=sh.iptables("--delete=FORWARD", "--in-interface=tun0","--jump=DROP")
      except:
        print ("Erreur suppression des règles de bases")
      try:
        out=sh.iptables("--delete=FORWARD", "--match=conntrack", "--ctstate=RELATED,ESTABLISHED", "--in-interface=tun0", "--jump=ACCEPT")
      except:
        print ("Erreur suppression des règles de bases")
        
        
      try:
        out=sh.iptables("--append=FORWARD", "--source="+ futureForwardingRules[numFutureRule]['ipUser'], "--destination="+ futureForwardingRules[numFutureRule]['ipEquipment'], "--in-interface=tun0", "--jump=ACCEPT")
      except:
        print ("Erreur ajout de règle Utilisateur") 
      
      
      try:
        out=sh.iptables("--append=FORWARD", "--jump=DROP", "--in-interface=tun0")
      except:
        print ("Erreur suppression des règles de bases")
      try:
        out=sh.iptables("--insert=FORWARD", "--match=conntrack", "--ctstate=RELATED,ESTABLISHED", "--jump=ACCEPT", "--in-interface=tun0")
      except:
        print ("Erreur suppression des règles de bases")
    
    numFutureRule += 1
#
# Etablie la connexion avec la base de données
#
#
def mySqlConnect():
  global dbMySql
  dbMySql= mysql.connector.connect(host="localhost",user="root",password="xal2017", database="sinass")
  dbMySql.autocommit=True
#
# Ferme la connexion avec la base de données
#
#
def mySqlClosed():
  global dbMySql
  dbMySql.close()
#
# Retourne les paramètres du fichier .env de Sinass
#
#
def getSinassParameters():
	sinassParams={}
	fichier=open("/home/ubuntu/sinass/.env", "r")
	for line in fichier.readlines():
		if (line.strip()!=''):
			param=line.strip().split('=')
			if (len(param)==2):
				sinassParams[param[0]]=param[1]
	fichier.close()
	return sinassParams


# --------------------Intialisation --------------------
print("Demarrage de la sectorisation OpenVPN")
SinassParameters=getSinassParameters()

mySqlConnect()

try:
  out=sh.sysctl("-w net.ipv4.ip_forward=1")
except:
  print ("Erreur activation des redirections")
try:
  out=sh.iptables("--delete=FORWARD", "--in-interface=tun0","--jump=DROP")
except:
  print ("Erreur suppression des règles de bases")
try:
  out=sh.iptables("--delete=FORWARD", "--match=conntrack", "--ctstate=RELATED,ESTABLISHED", "--in-interface=tun0", "--jump=ACCEPT")
except:
  print ("Erreur suppression des règles de bases")
try:
  out=sh.iptables("--append=FORWARD", "--jump=DROP", "--in-interface=tun0")
except:
  print ("Erreur suppression des règles de bases")
try:
  out=sh.iptables("--insert=FORWARD", "--match=conntrack", "--ctstate=RELATED,ESTABLISHED", "--jump=ACCEPT", "--in-interface=tun0")
except:
  print ("Erreur suppression des règles de bases")
  
while True: 
  #os.system("clear")
  #print('---------- Paramètres Sinass ----------')
  #print(SinassParameters['SINASS_NB_SIMULT_CONNECTIONS'])
  vpnClients=getConnectedClients()
  vpnUsersClients=getConnectedUsersClients(vpnClients)
  #print('---------- Utilisateurs ----------')
  #print('Utilisateurs connectés: '+ str(vpnUsersClients))
  #print(' ')
  #print('---------- Regles Iptables ----------')
  futureRules=(getEquipmentsConnectedUsers(vpnUsersClients))
  #print('Règles à utiliser: '+ str(futureRules))
  actualRules=getForwardingRules()
  deleteForwardingRules(actualRules,futureRules)
  addForwardingRules(actualRules,futureRules)
  time.sleep(1)
  
mySqlClosed()
  


