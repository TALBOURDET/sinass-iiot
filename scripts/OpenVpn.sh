#!/bin/sh
OPENVPND="/etc/openvpn"
DATASD="/home/ubuntu/Sinass-datas"
LOGSF="infos.log"
#echo "" > $DATASD/openvpn/$LOGSF
#if [ -f $DATASD/openvpn/$LOGSF ]; then
#        rm $DATASD/openvpn/$LOGSF
#fi
saveServerDir () {
    echo "* Sauvegarde du repertoire openvpn" >>$DATASD/openvpn/$LOGSF 2>&1
	cp -rp /etc/openvpn $DATASD/openvpn/serverConf/ >>$DATASD/openvpn/$LOGSF 2>&1
}
clientParamConf () {
    if [ -f /etc/openvpn/jail/ccd/${1} ]; then
        rm /etc/openvpn/jail/ccd/${1}
    fi
    if [ ! -d $DATASD/openvpn/clientsConf/${1} ]; then
        mkdir $DATASD/openvpn/clientsConf/${1} >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    if [ ! -d $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt ]; then
        mkdir $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    if [ -f $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/client.conf ]; then
        rm $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/client.conf
    fi
    if [ -f $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/client.ovpn ]; then
        rm $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/client.ovpn
    fi


    if [ ! -z ${3} ] && [ ! -z ${4} ]; then
        echo "ifconfig-push ${3} ${4}" > /etc/openvpn/jail/ccd/${1}
    fi
    
		#
    # Récupération du modèle de fichier de configuration client
    #
    cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf >>$DATASD/openvpn/$LOGSF 2>&1
		
		#
    # Définition de la localisation des certificats et clés
    #
    if [ ! -z ${5} ] && [ ! -z ${6} ] && [ ! -z ${7} ]; then
        sed -i -e "s#^ca ca.crt#ca ${5}/ca.crt#g" $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf >>$DATASD/openvpn/$LOGSF 2>&1
        sed -i -e "s#^cert client.crt#cert ${6}/${1}.crt#g" $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf >>$DATASD/openvpn/$LOGSF 2>&1
        sed -i -e "s#^key client.key#key ${7}/${1}.key#g" $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf >>$DATASD/openvpn/$LOGSF 2>&1
    else
        sed -i -e "s/^cert client.crt/cert ${1}.crt/g" $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf >>$DATASD/openvpn/$LOGSF 2>&1
        sed -i -e "s/^key client.key/key ${1}.key/g" $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    sed -i -e "s/^remote my-server-1 1194/remote ${2} 1194/g" $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf >>$DATASD/openvpn/$LOGSF 2>&1

    #
    # Activation du chiffrement AES-256-CBC 256
    #
    sed -i -e "s/^\;cipher x/cipher AES-256-CBC/g" $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf 2>&1

    cp $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.conf $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.ovpn >>$DATASD/openvpn/$LOGSF 2>&1
}
clientCreateCert () {
    #
    # Modification de KEY_CN
    #
    sed -i -e "s/export KEY_CN=\"Server\"/export KEY_CN=\"${1}\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1

    cd /etc/openvpn/sinass_ca
    echo "* Chargement des variables" >>$DATASD/openvpn/$LOGSF 2>&1
    . ./vars >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* re-intitialisation du fichier variables" >>$DATASD/openvpn/$LOGSF 2>&1
    sed -i -e "s/export KEY_CN=\"${1}\"/export KEY_CN=\"Server\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Génération du .crt et .key" >>$DATASD/openvpn/$LOGSF 2>&1
    ./pkitool ${1} >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Rangement de fichiers gÃ©nÃ©rÃ©s" >>$DATASD/openvpn/$LOGSF 2>&1
    cp /etc/openvpn/ca.crt /etc/openvpn/ta.key keys/${1}.crt keys/${1}.key $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt >>$DATASD/openvpn/$LOGSF 2>&1
	echo "* Rangement du fichier de variables" >>$DATASD/openvpn/$LOGSF 2>&1
    saveServerDir
}
clientCreateConfCert () {
    cp $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.ovpn $DATASD/openvpn/clientsConf/${1}/${1}.ovpn

    if [ ! -z ${2} ] && [ ! -z ${3} ] && [ ! -z ${4} ]; then
        sed -i -e "s#^ca ${2}/ca.crt#;ca ${2}/ca.crt#g" $DATASD/openvpn/clientsConf/${1}/${1}.ovpn >>$DATASD/openvpn/$LOGSF 2>&1
        sed -i -e "s#^cert ${3}/${1}.crt#;cert ${3}/${1}.crt#g" $DATASD/openvpn/clientsConf/${1}/${1}.ovpn >>$DATASD/openvpn/$LOGSF 2>&1
        sed -i -e "s#^key ${4}/${1}.key#;key ${4}/${1}.key#g" $DATASD/openvpn/clientsConf/${1}/${1}.ovpn >>$DATASD/openvpn/$LOGSF 2>&1
    else
        sed -i -e "s/^ca ca.crt/;ca ca.crt/g" $DATASD/openvpn/clientsConf/${1}/${1}.ovpn >>$DATASD/openvpn/$LOGSF 2>&1
        sed -i -e "s/^cert ${1}.crt/;cert ${1}.crt/g" $DATASD/openvpn/clientsConf/${1}/${1}.ovpn >>$DATASD/openvpn/$LOGSF 2>&1
        sed -i -e "s/^key ${1}.key/;key ${1}.key/g" $DATASD/openvpn/clientsConf/${1}/${1}.ovpn >>$DATASD/openvpn/$LOGSF 2>&1
    fi



    echo "<ca>" >> $DATASD/openvpn/clientsConf/${1}/${1}.ovpn
    cat $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/ca.crt >> $DATASD/openvpn/clientsConf/${1}/${1}.ovpn
    echo "</ca>\n<cert>" >> $DATASD/openvpn/clientsConf/${1}/${1}.ovpn
    cat $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.crt >> $DATASD/openvpn/clientsConf/${1}/${1}.ovpn
    echo "</cert>\n<key>" >> $DATASD/openvpn/clientsConf/${1}/${1}.ovpn
    cat $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/${1}.key >> $DATASD/openvpn/clientsConf/${1}/${1}.ovpn
    echo "</key>" >> $DATASD/openvpn/clientsConf/${1}/${1}.ovpn
    #echo "</key>\n<tls-auth>" >> $DATASD/openvpn/clientsConf/${1}/client-full.ovpn
    #cat $DATASD/openvpn/clientsConf/${1}/ConfKeyCrt/ta.key >> $DATASD/openvpn/clientsConf/${1}/client-full.ovpn
    #echo "</tls-auth>" >> $DATASD/openvpn/clientsConf/${1}/client-full.ovpn

}
clientUpdateIP () {
    echo "ifconfig-push ${2} ${3}" > /etc/openvpn/jail/ccd/${1}
	saveServerDir
    serveurReload
    echo "done"
}
serveurStart () {
    systemctl start openvpn@server.service >>$DATASD/openvpn/$LOGSF
}
serveurStop () {
    systemctl stop openvpn@server.service >>$DATASD/openvpn/$LOGSF
}
serveurReload () {
    systemctl restart openvpn@server.service >>$DATASD/openvpn/$LOGSF
}
serveurInitConf () { 
    #
    # Suppression des configuration précédentes
    #
    rm -R $DATASD/openvpn
    mkdir $DATASD/openvpn
    mkdir $DATASD/openvpn/clientsConf
    #mkdir $DATASD/openvpn/clientsConf/ccd
    #mkdir $DATASD/openvpn/clientsConf/crl
    mkdir $DATASD/openvpn/serverConf
    touch $DATASD/openvpn/$LOGSF
    #
    # Suppression des éléments du répertoire ccd
    #
    if [ -d /etc/openvpn/jail/ccd ]; then
        rm -R /etc/openvpn/jail/ccd >>$DATASD/openvpn/$LOGSF 2>&1
        mkdir /etc/openvpn/jail/ccd >>$DATASD/openvpn/$LOGSF 2>&1
    fi
		#
    # initialisation du fichier de revocation
    #
    if [ -f /etc/openvpn/jail/crl.pem ]; then
        rm /etc/openvpn/jail/crl.pem
				touch /etc/openvpn/jail/crl.pem
    fi
    #
    # Suppression des configuration serveur
    #
    if [ -f /etc/openvpn/ca.crt ]; then
        rm /etc/openvpn/ca.crt
    fi

    if [ -f /etc/openvpn/server.crt ]; then
        rm /etc/openvpn/server.crt
    fi

    if [ -f /etc/openvpn/server.key ]; then
        rm /etc/openvpn/server.key
    fi

    if [ -f /etc/openvpn/ta.key ]; then
        rm /etc/openvpn/ta.key
    fi

    if [ -f /etc/openvpn/dh2048.pem ]; then
        rm /etc/openvpn/dh2048.pem
    fi

    if [ -f /etc/openvpn/server.conf ]; then
        rm /etc/openvpn/server.conf
    fi
}
clientToClient () {
		serveurStop
		if [ ${1} = "true" ]; then
				sed -i -e "s/^\;client-to-client/client-to-client/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
		fi
		if [ ${1} = "false" ]; then
				sed -i -e "s/^client-to-client/\;client-to-client/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
		fi
		serveurStart
}
serveurParamConf () {
    #
    # Installation du fichier de logs
    #
    if [  -f $DATASD/openvpn/$LOGSF ]; then
        rm $DATASD/openvpn/$LOGSF
    fi
    
    if [ ! -f $DATASD/openvpn/$LOGSF ]; then
        touch $DATASD/openvpn/$LOGSF
    fi
    #
    # Installation du fichier de configuration
    #
    if [  -f /etc/openvpn/server.conf ]; then
        rm /etc/openvpn/server.conf
        zcat /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz > /etc/openvpn/server.conf
    fi
		
		if [ ! -f /etc/openvpn/server.conf ]; then
        zcat /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz > /etc/openvpn/server.conf
    fi
    #
    # Modification de la plage d'adresse du rÃ©seau VPN
    #
    if [ ! -z ${1} ] && [ ! -z ${2} ]; then 
        ipServer="server $1 $2"
        sed -i -e "s/server 10.8.0.0 255.255.255.0/$ipServer/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Autorisation ou non de la communication inter clients 
    #
    #
    if [ ${3} = "false" ]; then 
        sed -i -e "s/^\;client-to-client/\;client-to-client/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
    else
        sed -i -e "s/^\;client-to-client/client-to-client/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Activation topology subnet
    #
    sed -i -e "s/^\;topology subnet/topology subnet/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
		#
    # Activation du chiffrement AES-256-CBC 256
    #
    sed -i -e "s/^\;cipher BF-CBC/cipher AES-256-CBC/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
    #
    # Chroot de OpenVPN dans le rÃ©pertoire jail
    #
    sed -i "/;mute 20/ a\chroot /etc/openvpn/jail" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
    sed -i "/;mute 20/a \\\n# Securisation de Open VPN grace Ã  un chroot dans repertoire jail" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
    
    echo "client-config-dir ccd" >> /etc/openvpn/server.conf 2>&1
    #echo "ccd-exclusive" >> /etc/openvpn/server.conf 2>&1
    echo "crl-verify crl.pem" >> /etc/openvpn/server.conf 2>&1
    #
    # Activation des logs
    #
    #sed -i -e "s/^\;log-append  openvpn.log/log-append  openvpn.log/g" /etc/openvpn/server.conf >>$DATASD/openvpn/$LOGSF 2>&1
    
}
serveurCreateCert () {
    #
    # RÃ©cupÃ©ration du fichier modÃ¨le  pour le configuration serveur et destruction de l'ancien fichier de configuration
    #
    if [ -f /etc/openvpn/sinass_ca/template-vars ]; then
        rm /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
        cp /etc/openvpn/sinass_ca/template-vars /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Modification de KEY_COUNTRY
    #
    if [ ! -z ${1} ]; then
        sed -i -e "s/export KEY_COUNTRY=\"US\"/export KEY_COUNTRY=\"${1}\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Modification de KEY_PROVINCE
    #
    if [ ! -z ${2} ]; then
        sed -i -e "s/export KEY_PROVINCE=\"CA\"/export KEY_PROVINCE=\"${2}\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Modification de KEY_CITY
    #
    if [ ! -z ${3} ]; then
        sed -i -e "s/export KEY_CITY=\"SanFrancisco\"/export KEY_CITY=\"${3}\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Modification de KEY_ORG
    #
    if [ ! -z ${4} ]; then
        sed -i -e "s/export KEY_ORG=\"Fort-Funston\"/export KEY_ORG=\"${4}\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Modification de KEY_EMAIL
    #
    if [ ! -z ${5} ]; then
        sed -i -e "s/export KEY_EMAIL=\"me@myhost.mydomain\"/export KEY_EMAIL=\"${5}\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    fi
    #
    # Modification de KEY_CN
    #
    sed -i -e "s/# export KEY_CN=\"CommonName\"/export KEY_CN=\"Server\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1

    cd /etc/openvpn/sinass_ca
    echo "* Chargement des variables" >>$DATASD/openvpn/$LOGSF
    . ./vars >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Suppression des anciennes clÃ©s et certificats" >>$DATASD/openvpn/$LOGSF
    ./clean-all >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Génération du Diffie Hellman" >>$DATASD/openvpn/$LOGSF
    ./build-dh >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Génération du CA" >>$DATASD/openvpn/$LOGSF
    ./pkitool --initca >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Génération du sertificat serveur" >>$DATASD/openvpn/$LOGSF 2>&1
    ./pkitool --server server >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Génération des clÃ©s" >>$DATASD/openvpn/$LOGSF 2>&1
    openvpn --genkey --secret keys/ta.key >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Copie des fichiers  (clés et certificats) dans /etc/openvpn" >>$DATASD/openvpn/$LOGSF 2>&1
    cp keys/ca.crt keys/ta.key keys/server.crt keys/server.key keys/dh2048.pem /etc/openvpn/ >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Rengement du fichier de variables" >>$DATASD/openvpn/$LOGSF 2>&1
    saveServerDir
}
serveur () {
    if [ $# = 8 ]; then
        serveurStop
        serveurInitConf
        serveurParamConf $1 $2 $3
        serveurCreateCert $4 $5 $6 $7 $8
        serveurStart
        echo "done"
    else
        echo "error"
    fi
}
client () {
    clientParamConf ${1} ${2} ${3} ${4} ${5} ${6} ${7}
    clientCreateCert ${1}
    clientCreateConfCert ${1} ${5} ${6} ${7}
    echo "done"
}
revokeClient () {
    #echo "* Arret du serveur open VPN" >>$DATASD/openvpn/$LOGSF 2>&1
    #/etc/init.d/openvpn stop >>$DATASD/openvpn/$LOGSF
    #
    # Modification de KEY_CN
    #
    sed -i -e "s/export KEY_CN=\"Server\"/export KEY_CN=\"${1}\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    dir=$PWD 
    cd /etc/openvpn/sinass_ca
    echo "* Chargement des variables" >>$DATASD/openvpn/$LOGSF 2>&1
    . ./vars >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* re-intitialisation du fichier variables" >>$DATASD/openvpn/$LOGSF 2>&1
    sed -i -e "s/export KEY_CN=\"${1}\"/export KEY_CN=\"Server\"/g" /etc/openvpn/sinass_ca/vars >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* GÃ©nÃ©ration de la revocation" >>$DATASD/openvpn/$LOGSF 2>&1
    ./revoke-full ${1} >>$DATASD/openvpn/$LOGSF 2>&1
    echo "* Placement du fichier de revocation" >>$DATASD/openvpn/$LOGSF 2>&1
    cp keys/crl.pem /etc/openvpn/jail/
    echo "* Redemarrage du serveur open VPN" >>$DATASD/openvpn/$LOGSF 2>&1
    serveurReload
    cd $dir
    echo "* Suppression des fichiers client" >>$DATASD/openvpn/$LOGSF 2>&1
    rm -R $DATASD/openvpn/clientsConf/${1}
    rm /etc/openvpn/jail/ccd/${1}
	saveServerDir
    echo "done"
}
clientsStates() {
    cat /etc/openvpn/openvpn-status.log
}
serverState() {
    if ! /sbin/ifconfig tun0 2>/dev/null | grep -q "00-00-00-00-00-00-00-00-00-00-00-00-00-00-00-00" 
    then
        echo down
    else
        echo up
    fi    
}
logs (){
    cat /etc/openvpn/openvpn.log
}
help () {
    echo ""
    echo "Srcipt de paramètrage d'OpenVPN pour Sinass V1"
    echo ""
    echo "-start    Demarre de serveur OpenVPN"
    echo ""
    echo "-stop     Arrête du serveur OpenVPN"
    echo ""
    echo "-reload   redemarrage du serveur OpenVPN"
    echo "" 
    echo "-s        Configuration serveur"
    echo "          -s (IP de départ) (Masque) (clientToClient true ou false) (KEY_COUNTRY) (KEY_PROVINCE) (KEY_CITY) (KEY_ORG) (KEY_EMAIL)"
    echo ""
    echo "-c        Configuration clients"
    echo "          -c (Nom client) (IP Publique Serveur) (IP Client) (Masque:q) (option: URL vers rep CA) (option: URL vers rep CERT) (option: URL vers rep Key)"
    echo ""
    echo "-cUIp     Mise à jours IP et Masque du client"
    echo ""
    echo "-r        Révocation clients"
    echo "          -r (Nom client)"
    echo ""
    echo "-logs     Logs du serveur"
    echo ""
    echo "-cStates  Diagnostic des clients"
    echo ""
	echo "-sState   Etat du serveur"
	echo ""
    echo "-cTc      Active ou désactive la fonctionnalité client-to-client"
    echo ""
	echo "-logs     Logs du serveur"
    echo ""
    echo "          Répertoire des données:"
    echo "          $DATASD"
    echo ""
}

if [ -z ${1} ]; then
   help 
fi

case ${1} in
    "-start") serveurStart 
   ;;
    "-stop") serveurStop 
   ;;
    "-reload") serveurReload 
   ;;
    "-s") serveur ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9}
   ;;
    "-c") client ${2} ${3} ${4} ${5} ${6} ${7} ${8} 
   ;;
    "-r") revokeClient ${2}
   ;;
    "-cStates") clientsStates
   ;;
    "-sState") serverState
	 ;;
    "-cTc") clientToClient ${2}
   ;;
   "-logs") logs 
   ;;
    "-help") help 
   ;;
    "--help") help 
   ;;
    "-cUIp") clientUpdateIP ${2} ${3} ${4}
esac