#!/bin/sh
DATASD="/home/webadmin/Sinass-datas"
LOGSD="/var/log/Sinass-logs"
LOGSF="Logs.log"

help () {
    echo ""
    echo "Srcipt de pilotage des services pour Sinass"
    echo ""
		echo "-start 		Demarre le service"
    echo "          -start (NomDuService)"
		echo ""
		echo "-stop 		Arrete le service"
    echo "          -stop (NomDuService)"
		echo ""
		echo "-firewall -getRules		liste les filtres actifs"
    echo ""
    echo "-status   Renvoie l'état d'un service"
    echo "          -status (NomDuService)"
    echo ""  
    echo "Fin"
}
servicesStart () {
  sudo systemctl start ${1}
}
servicesStop () {
  sudo systemctl stop ${1}
}
servicesStatus () {
  state=$(sudo systemctl is-active ${1})
 if [ $state = active ]; then
  echo "up"
 fi
 if [ $state = inactive ]; then
  echo "down"
 fi
}
insertRule () {
	state=$(sudo iptables -I INPUT -p ${1} --dport ${2} -j ACCEPT)	
}
removeRule () {
	state=$(sudo iptables -D INPUT ${1})	
}
firewall () {
	case ${1} in
		"-getRules") sudo iptables -L --line-numbers
		;;
		"-removeRule") sudo iptables -D INPUT ${2}
		;;
		"-insertRule") insertRule ${2} ${3}
		;;
	esac
}
if [ -z ${1} ]; then
   help 
fi

case ${1} in
    "-start") servicesStart ${2}
   ;;
    "-stop") servicesStop ${2}
   ;;
    "-status") servicesStatus ${2}
   ;;
    "-firewall") firewall ${2} ${3} ${4}
   ;;
    "--help") help 
   ;;
esac