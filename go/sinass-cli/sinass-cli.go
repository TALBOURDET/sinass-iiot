package main

// #include <stdlib.h>
//
// void clear() {
//	system("clear");
// }
import "C"

import(
	"fmt"
	"strings"
	"strconv"
	"os"
	"os/exec"
	"io/ioutil"
	"regexp"
	//"sinass/services"
)
const (
	VERSION string ="V1.0.0 beta"
	VHOST_AVAILABLE_DIR_PATH string = "/etc/apache2/sites-available/"
	VHOST_ENABLED_DIR_PATH string = "/etc/apache2/sites-enabled/"
	LETSENCRYPT_CERT_DIR_PATH string = "etc/letsencrypt/live/"
	NODE_RED_PARAM_PATH string = "/home/ubuntu/.node-red/settings.js"
	GRAFANA_PARAM_PATH string = "/etc/grafana/grafana.ini"
	OPENVPN_PATH string ="/etc/openvpn/"
	SINASS_ENV_PATH string = "/home/ubuntu/sinass/.env"
	SINASS_DATA_PATH string = "/home/ubuntu/Sinass-datas/"
)
func main() {
	
	var opt string
	var arg []string

	if len(os.Args)>1 {
		opt=os.Args[1]
		arg=os.Args[2:]
	} else {
		opt=""
		
	}

	router(opt,arg)
}

func router(opt string, arg []string){
	var out string
	view(out)
	switch opt {
	case "-help":
		out=help(arg)
	case "-status":
		out=status(arg)
	case "-node-red":
		out=nodeRed(arg)
	case "-grafana":
		out=grafana(arg)
	case "-openvpn":
		out=openvpn(arg)
	case "-mysql":
		out=mysql(arg)
	case "-apache":
		out=apache(arg)
	case "-influxdb":
		out=influxdb(arg)
	case "-start":
		out=start(arg)
	case "-stop":
		out=stop(arg)
	case "-restart":
		out=restart(arg)
	case "-domain":
		out=domain(arg)
	case "-get-db":
		out=getDbParameters(arg)
	case "-set-db":
		out=setDbParameters(arg)
	case "-debug-backend":
		out=cliSinassEnvParam(arg,"APP_DEBUG")
	case "-nb-users":
		out=cliSinassEnvParam(arg,"SINASS_NB_USERS")
	case "-nb-equipments":
		out=cliSinassEnvParam(arg,"SINASS_NB_EQUIPMENTS")
	case "-nb-simult":
		out=cliSinassEnvParam(arg,"SINASS_NB_SIMULT_CONNECTIONS")
	case "-opt-grafana":
		out=cliSinassEnvParam(arg,"SINASS_GRAFANA")
	case "-opt-node-red":
		out=cliSinassEnvParam(arg,"SINASS_NODERED")
	case "-init-openvpn":
		out=initOpenvpn(arg)
	case "-failover":
		out=setFailover(arg)
	case "-list-hdd":
		out=listHdd(arg)
	case "-mount-hdd":
		out=mountHdd(arg)
	case "-restore":
		out=restoreOpenVpn(arg)
	default:
		out=help(arg)	
	}

	fmt.Println(out)
	
}
func view(out string){
	C.clear()
	//services.Aa = "bijour"
	//aa.sererIp = "aa"

	//var openVpn services.Service
	//openVpn.name="openvpn"

	//fmt.Println(openVpn.GetStatus())


	//tap.ServerIp = "aa"


	//fmt.Println(tap.ServerIp)
	//services.Test()
	fmt.Println("// SINASS CLI " + VERSION)
	fmt.Println("")
}
func help(arg []string) string {
	fmt.Println(`Documentation:

 -status                      Affiche l'état des différent services sinass

 -node-red [nombre de logs]   Affiche le diagnostic de l'application node-red
 -grafana  [nombre de logs]   Affiche le diagnostic de l'application grafana
 -openvpn  [nombre de logs]   Affiche le diagnostic de l'application openvpn
 -mysql    [nombre de logs]   Affiche le diagnostic de l'application MySQL
 -sectorisation (En attente de developpement)
 
 -start [service]    Demarre un service (node-red, grafana, openvpn, sectorisation, apache, mysql)
 -stop [service]     Arrete un service (node-red, grafana, openvpn, sectorisation, apache, mysql)
 -restart [service]  Redemarre un service (node-red, grafana, openvpn, sectorisation, apache, mysql)

 -failover [ipfailover]           Paramttre l'IP Failover 
 -domain   [domain] [true|false]  Permet de mettre à jour le nom de domaine de l'application sinass
								  Le dexieme paramètre active ou non le https

 -get-db                          Récupères les paramètres de connection de la base de donnée interne SINASS 
 -set-db [DB_CONNECTION] [DB_HOST] [DB_PORT] [DB_DATABASE] [DB_USERNAME] [DB_PASSWORD]
                                  Modifie les paramètres de connection de la base de donnée interne SINASS

 -debug-backend [true|false] Active ou desactive le mode debug du backend

 -nb-users      [nb]            Nombre maximum d'utilisateur pour l'application (param optionnel)
 -nb-equipments [nb]            Nombre maximum d'équipements pour l'application (param optionnel)
 -nb-simult     [nb]            Nombre maximum de connexions simultanés pour le vpn (ne fonctionne qu'avec la sectorisation) (param optionnel)
 -opt-grafana   [true|false]    Active ou desative la possibilité d'utiliser Grafana (param optionnel)
 -opt-node-red  [true|false]    Active ou desative la possibilité d'utiliser Node-Red (param optionnel)

 -init-openvpn  [IP_RESEAU] [MASQUE] [CLIENT_TO_CLIENT] [KEY_COUNTRY] [KEY_PROVINCE] [KEY_CITY] [KEY_ORG] [KEY_MAIL] 
                Initialise le serveur Open-VPN
                - IP_RESEAU Ex: "192.168.1.0"  pour un premier equipement en 192.168.1.1
                - CLIENT_TO_CLIENT doit correspondre à true ou false
                - KEY_COUNTRY  Ex: "FR", contient les deux premières lettre du nom 
                - KEY_PROVINCE  Ex: "PACA", ne doit par contenir d'espaces ou accents
                - KEY_CITY  Ex: "AVIGNON", ne doit par contenir d'espaces ou accents
                - KEY_ORG  Ex: "IDSU", ne doit par contenir d'espaces ou accents
                - KEY_MAIL  Ex: "contact@idsu.fr", ne doit par contenir d'espaces ou accents

 -list-hdd                      Liste les disques
 -mount-hdd     [disque]        Monte le disque donné en paramètre en tant que Sinass-datas

 -restore       Restore les paramètres depuis le disque
`)
//  -parameter               Affiche les paramètres de l'application SINASS
//  -init-db                 Initialise la base de données
//  -mail [MAIL_DRIVER] [MAIL_HOST] [MAIL_PORT] [MAIL_USERNAME] [MAIL_PASSWORD] [MAIL_ENCRYPTION]
// `
return ""
}
func status(arg []string) string{
	out, _ := exec.Command("systemctl", "is-active", "openvpn").Output()
	fmt.Println("OpenVPN: " + string(out))
	out, _ = exec.Command("systemctl", "is-active", "SectorisationVpn").Output()
	fmt.Println("Sectorisation Open-VPN: " + string(out))
	out, _ = exec.Command("systemctl", "is-active", "grafana-server").Output()
	fmt.Println("Grafana: " + string(out))
	out, _ = exec.Command("systemctl", "is-active", "node-red").Output()
	fmt.Println("Node-Red: " + string(out))
	out, _ = exec.Command("systemctl", "is-active", "mysql").Output()
	fmt.Println("MySQL: " + string(out))
	out, _ = exec.Command("systemctl", "is-active", "apache2").Output()
	fmt.Println("Apache: " + string(out))
	out, _ = exec.Command("systemctl", "is-active", "influxdb").Output()
	fmt.Println("InfluxDB: " + string(out))
	return ""
}
func grepLLogs (pattern,file, num string) string {
	grep:= exec.Command("grep", pattern, file)
	tail:= exec.Command("tail", num)
	grepOut, _ := grep.StdoutPipe()
	grep.Start()
	tail.Stdin = grepOut
	out, _ := tail.Output()

	return string(out)
}
func lsFilterNot (dir,filter string, invert bool) ([]string, int) {
	// Compte les fichiers correspondants au filtre
	ls:= exec.Command("ls", dir)
	lsOut, _ := ls.StdoutPipe()

	var grep *exec.Cmd;

	if (invert==true) {
		grep= exec.Command("grep", "-v", filter)
	} else {
		grep= exec.Command("grep", filter)
	}
	grep.Stdin = lsOut
	grepOut, _ := grep.StdoutPipe()

	wc:= exec.Command("wc", "-l")
	wc.Stdin = grepOut

	
	ls.Start()
	
	grep.Start()
	
	
	outNum, _ := wc.Output()
		
	outNum=outNum[:len(outNum)-1] // suppression du retour chariot
	int64Num, _ := strconv.ParseInt(string(outNum), 10, 64)
	
	num:=int(int64Num)

	// Liste les fichiers correspondants au filtre
	ls= exec.Command("ls", dir)
	lsOut, _ = ls.StdoutPipe()

	if (invert==true) {
		grep= exec.Command("grep", "-v", filter)
	} else {
		grep= exec.Command("grep", filter)
	}
	grep.Stdin = lsOut
	
	ls.Start()
	
	byteOutFile, _ := grep.Output()
	files:=strings.Split(string(byteOutFile),"\n")
	

	return files, num
}
func nodeRed (arg []string) string {
	if len(arg)==0 {
		arg=append(arg,"10")
	}
	nodeRed:=grepLLogs("node-red","/var/log/syslog","-"+arg[0])
	return "Logs du service Node-Red\n\n" + nodeRed
}
func openvpn (arg []string) string {
	if len(arg)==0 {
		arg=append(arg,"10")
	}
	openvpn:=grepLLogs("ovpn-server","/var/log/syslog","-"+arg[0])
	return "Logs du service OpenVPN\n\n" + openvpn
}
func grafana (arg []string) string {
	if len(arg)==0 {
		arg=append(arg,"10")
	}
	grafana:=grepLLogs("grafana-server","/var/log/syslog","-"+arg[0])
	return "Logs du service Grafana\n\n" + grafana
}
func mysql (arg []string) string {
	if len(arg)==0 {
		arg=append(arg,"10")
	}
	mysql:=grepLLogs("","/var/log/mysql/error.log","-"+arg[0])
	return "Logs du service MySQL\n\n" + mysql
}
func apache (arg []string) string {
	if len(arg)==0 {
		arg=append(arg,"10")
	}
	apache:=grepLLogs("","/var/log/apache2/error.log","-"+arg[0])
	return "Logs du service Apache\n\n" + apache
}
func influxdb (arg []string) string {
	if len(arg)==0 {
		arg=append(arg,"10")
	}
	influxdb:=grepLLogs("influxdb","/var/log/syslog","-"+arg[0])
	return "Logs du service InfluxDB\n\n" + influxdb
}
func start (arg []string) string {
	serviceName:=""
	switch arg[0] {
	case "node-red":
		serviceName="node-red"
	case "grafana":
		serviceName="grafana-server"
	case "openvpn":
		serviceName="openvpn"
	case "sectorisation":
		serviceName="SectorisationVpn"
	case "apache":
		serviceName="apache2"
	case "mysql":
		serviceName="mysql"
	case "influxdb":
		serviceName="influxdb"
	default:
		fmt.Println("Le nom du service indiqué est incorrect")
		return ""
	}
	out, _ := exec.Command("sudo", "systemctl", "start", serviceName ).Output()
	fmt.Println("Demarrage du service: "+arg[0])
	fmt.Println(string(out))
	
	return ""
}
func stop (arg []string) string {
	serviceName:=""
	switch arg[0] {
	case "node-red":
		serviceName="node-red"
	case "grafana":
		serviceName="grafana-server"
	case "openvpn":
		serviceName="openvpn"
	case "sectorisation":
		serviceName="SectorisationVpn"
	case "apache":
		serviceName="apache2"
	case "mysql":
		serviceName="mysql"
	case "influxdb":
		serviceName="influxdb"
	default:
		fmt.Println("Le nom du service indiqué est incorrect")
		return ""
	}
	out, _ := exec.Command("sudo", "systemctl", "stop", serviceName ).Output()
	fmt.Println("Arrêt du service: "+arg[0])
	fmt.Println(string(out))
	
	return ""
}
func restart (arg []string) string {
	strOut:=""
	switch arg[0] {
	case "node-red":
		out, _ := exec.Command("sudo", "systemctl", "restart", "node-red" ).Output()
		strOut=string(out)
	case "grafana":
		out, _ := exec.Command("sudo", "systemctl", "restart", "grafana-server" ).Output()
		strOut=string(out)
	case "openvpn":
		out, _ := exec.Command("sudo", "systemctl", "restart", "openvpn" ).Output()
		strOut=string(out)
	case "sectorisation":
		out, _ := exec.Command("sudo", "systemctl", "restart", "SectorisationVpn" ).Output()
		strOut=string(out)
	case "apache":
		out, _ := exec.Command("sudo", "systemctl", "restart", "apache2" ).Output()
		strOut=string(out)
	case "mysql":
		out, _ := exec.Command("sudo", "systemctl", "restart", "mysql" ).Output()
		strOut=string(out)
	default:
		return "Le service indiqué est incorrect"
	}
	return "Redemarrage de " + arg[0] + "\n" + strOut
}
// type domainConf struct {
//     domainName string
// }
// func getActualDomain() string {

// }
func check(e error) {
    if e != nil {
		fmt.Println(e)
        panic(e)
    }
}
func checkInFile(file, textToCheck string) bool {
	dat, err := ioutil.ReadFile(file)
	if err ==nil {
		text := string(dat)
		num:=strings.Count(text,textToCheck)
		if num>0 {
			return true
		}
	}
	return false
}
func addLineInFile(file, lineToAdd string) error {
	dat, err := ioutil.ReadFile(file)
	if err ==nil {
		text := string(dat)
		text = text + "\n" + lineToAdd + "\n"
		err = ioutil.WriteFile(file,[]byte(text),0777)
	} else {
		text := lineToAdd + "\n"
		err = ioutil.WriteFile(file,[]byte(text),0777)
	}
	return err
}
func replaceInFile(file, old, new string) (bool, error) {
	status:=false
	dat, err := ioutil.ReadFile(file)
	if err ==nil {
		text := string(dat)
		num:=strings.Count(text,old)
		if num>0 {
			status=true
		} else {
			status=false
		}
		text = strings.Replace(text, old, new, num)
		err = ioutil.WriteFile(file,[]byte(text),0777)
	} else {
		status =false
	}
	return status, err
}
func setGrafanaDomain(actualDomain, futureDomain string) {
	fmt.Println(" * Modification paramètre de Grafana")
	replaceInFile(GRAFANA_PARAM_PATH,actualDomain,futureDomain)
}
func setNodeRedDomain(actualDomain, futureDomain string) {
	fmt.Println(" * Modification paramètre de Node-Red")
	replaceInFile(NODE_RED_PARAM_PATH,actualDomain,futureDomain)
}
func getSinassEnvParam(envFile, paramName string) string{
	param:=testRegEx(envFile,paramName + "=.+")
	return strings.Split(param,"=")[1]
}
func setSinassEnvParam(envFile, paramName, value string) string{
	textToReplace:=testRegEx(envFile,paramName + "=.+")
	replaceInFile(envFile,textToReplace,paramName + "=" + value)
	return value
}
func setSetSinassConfParam(confFile, paramName, value string) error{
	textToReplace:=testRegEx(confFile,paramName + "=.+")
	status, err:=replaceInFile(confFile,textToReplace,paramName + "=" + value)
	if status==false {
		err=addLineInFile(confFile, paramName + "=" + value) 
	} 
	return err
}
func setApacheDomain(actualDomain, futureDomain, https string) {
	fmt.Println(" * Modification des paramètres de Apache")
	replaceInFile(VHOST_AVAILABLE_DIR_PATH + actualDomain + ".conf", actualDomain, futureDomain)
	replaceInFile(VHOST_AVAILABLE_DIR_PATH + actualDomain + ".conf", "Include /etc/letsencrypt/options-ssl-apache.conf\n", "")
	replaceInFile(VHOST_AVAILABLE_DIR_PATH + actualDomain + ".conf", "SSLCertificateFile /etc/letsencrypt/live/"+futureDomain+"/fullchain.pem\n", "")
	replaceInFile(VHOST_AVAILABLE_DIR_PATH + actualDomain + ".conf", "SSLCertificateKeyFile /etc/letsencrypt/live/"+futureDomain+"/privkey.pem\n", "")
	os.Rename(VHOST_AVAILABLE_DIR_PATH + actualDomain + ".conf", VHOST_AVAILABLE_DIR_PATH + futureDomain + ".conf")
	os.Remove(VHOST_ENABLED_DIR_PATH + actualDomain + ".conf")
	os.Symlink(VHOST_AVAILABLE_DIR_PATH + futureDomain + ".conf",VHOST_ENABLED_DIR_PATH + futureDomain + ".conf")
	exec.Command("certbot", "delete", "--cert-name", actualDomain).Output()
	if https=="true" {
		replaceInFile(VHOST_AVAILABLE_DIR_PATH + futureDomain + ".conf", "#Redirectpermanent / https://"+futureDomain+"/", "Redirectpermanent / https://"+futureDomain+"/")
		fmt.Println(" * Création de certificats HTTPS")
		fmt.Println(" * Toutes les connexion HTTP seront redirigé vers HTTPS")
		out, _ :=exec.Command("certbot", "-n", "--apache", "-d", futureDomain).Output()
		fmt.Println(string(out))
	} else {
		fmt.Println(" * Pas de création de certificats")
		fmt.Println(" * Les connexions HTTP seronts autorisés")
		replaceInFile(VHOST_AVAILABLE_DIR_PATH + futureDomain + ".conf", "#Redirectpermanent / https://"+futureDomain+"/", "Redirectpermanent / https://"+futureDomain+"/")
		replaceInFile(VHOST_AVAILABLE_DIR_PATH + futureDomain + ".conf", "Redirectpermanent / https://"+futureDomain+"/", "#Redirectpermanent / https://"+futureDomain+"/")	
	}
	restart([]string{"apache"})
}
func domain(arg []string) string {
	file, num:=lsFilterNot(VHOST_AVAILABLE_DIR_PATH, "default",true)
	if 0 < num && num < 2 {
		fmt.Println("Nouveau domaine: "+ arg[0])
		domain:=strings.Split(file[0], ".conf")[0]
		setApacheDomain(domain,arg[0],arg[1])
		setGrafanaDomain(domain,arg[0])
		setNodeRedDomain(domain,arg[0])
		setSinassEnvParam(SINASS_ENV_PATH,"SINASS_URL",arg[0])
		setSinassEnvParam(SINASS_ENV_PATH,"APP_URL",arg[0])
	} else {
		fmt.Println("error")
	}
	return string(strconv.Itoa(num))
}
func testRegEx(file,search string) string{
	dat, err := ioutil.ReadFile(file)
	//var text string
	//if err ==nil {
	text := string(dat)
	re, err := regexp.Compile(search)
	check(err)
		//text="aa"
		//return search
	return re.FindString(text)
	//}
	//text="aa"
	//return "error"
}
func setDbParameters(arg []string) string{
	if len(arg)==6 {
		fmt.Println("Modification des paramètres de connexion à la base de donné interne SINASS")
		fmt.Println("")
		fmt.Println(" * DB_CONNECTION: "+ setSinassEnvParam(SINASS_ENV_PATH,"DB_CONNECTION",arg[0]))
		fmt.Println(" * DB_HOST:       "+ setSinassEnvParam(SINASS_ENV_PATH,"DB_HOST",arg[1]))
		fmt.Println(" * DB_PORT:       "+ setSinassEnvParam(SINASS_ENV_PATH,"DB_PORT",arg[2]))
		fmt.Println(" * DB_DATABASE:   "+ setSinassEnvParam(SINASS_ENV_PATH,"DB_DATABASE",arg[3]))
		fmt.Println(" * DB_USERNAME:   "+ setSinassEnvParam(SINASS_ENV_PATH,"DB_USERNAME",arg[4]))
		fmt.Println(" * DB_PASSWORD:   "+ setSinassEnvParam(SINASS_ENV_PATH,"DB_PASSWORD",arg[5]))
	} else {
		fmt.Println("Six paramètres sont attendu")
	}
	

	return ""
}
func getDbParameters(arg []string) string{
	fmt.Println(" * DB_CONNECTION: "+ getSinassEnvParam(SINASS_ENV_PATH,"DB_CONNECTION"))
	fmt.Println(" * DB_HOST:       "+ getSinassEnvParam(SINASS_ENV_PATH,"DB_HOST"))
	fmt.Println(" * DB_PORT:       "+ getSinassEnvParam(SINASS_ENV_PATH,"DB_PORT"))
	fmt.Println(" * DB_DATABASE:   "+ getSinassEnvParam(SINASS_ENV_PATH,"DB_DATABASE"))
	fmt.Println(" * DB_USERNAME:   "+ getSinassEnvParam(SINASS_ENV_PATH,"DB_USERNAME"))
	fmt.Println(" * DB_PASSWORD:   "+ getSinassEnvParam(SINASS_ENV_PATH,"DB_PASSWORD"))

	return ""
}
func cliSinassEnvParam(arg []string,paramName string) string{
	fmt.Println("Paramètre SINASS:")

	if len(arg)==1 {
		fmt.Println(" * "+paramName+": "+ setSinassEnvParam(SINASS_ENV_PATH,paramName,arg[0]))
	}else{
		fmt.Println(" * "+paramName+": "+ getSinassEnvParam(SINASS_ENV_PATH,paramName))
	}
	return ""
}
func initOpenvpn(arg []string) string{
	if len(arg)==8 {
		fmt.Println("Initialisation serveur Open-VPN:")
		exec.Command("OpenVpn", "-s", arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], arg[6], arg[7]).Output()
		fmt.Println("Logs disponibles dans Sinass-datas/LogSinass")
	} else {
		fmt.Println("Huit paramètres sont attendu")
	}
	return ""
}
func setFailover(arg []string) string{
	fmt.Println(" * Modification du /etc/network/interfaces.d/failover.cfg")
	param:=testRegEx("/etc/network/interfaces.d/failover.cfg","address "+ ".+")
	replaceInFile("/etc/network/interfaces.d/failover.cfg", param, "address "+ arg[0])
	param=testRegEx("/etc/network/interfaces.d/failover.cfg","broadcast "+ ".+")
	replaceInFile("/etc/network/interfaces.d/failover.cfg", param, "broadcast "+ arg[0])
	fmt.Println(" * Redemarrage interface")
	exec.Command("/etc/init.d/networking", "restart").Output()

	return ""
}
func listHdd(arg []string) string{
	out, _ := exec.Command("lsblk").Output()
	fmt.Println(string(out))

	return ""
}
func mountHdd(arg []string) string{
	//sudo mount /dev/vdb1 /mnt/disk/
	out, _ := exec.Command("mount","/dev/" + arg[0],"/home/ubuntu/Sinass-datas").Output()
	fmt.Println("Montage de Sinass-datas avec " + arg[0])
	fmt.Println(string(out))

	return ""
}

//func saveOpenVpn(arg []string) string{
//	exec.Command("cp", OPENVPN_PATH+"server.conf", OPENVPN_PATH+"sinass_ca/keys/ca.crt", OPENVPN_PATH+"sinass_ca/keys/ta.key", OPENVPN_PATH+"sinass_ca/keys/server.crt", OPENVPN_PATH+"sinass_ca/keys/server.key", OPENVPN_PATH+"sinass_ca/keys/dh2048.pem", SINASS_DATA_PATH+"openvpn/serverConf").Output()
//	exec.Command("cp", OPENVPN_PATH+"jail/ccd/*", , SINASS_DATA_PATH+"openvpn/clientsConf/ccd/).Output()
//	exec.Command("cp", OPENVPN_PATH+"jail/ccd/*", , SINASS_DATA_PATH+"openvpn/clientsConf/ccd/).Output()	
//}

func Exists(name string) (bool, error) {
  _, err := os.Stat(name)
  if os.IsNotExist(err) {
    return false, nil
  }
  return err != nil, err
}
func restoreOpenVpn(arg []string) string{
	// Suppresssion des anciens certificats et fichiers de configuaration
	fmt.Println(" * Suppresssion des anciens certificats et fichiers de configuaration")
	var err error 
	exec.Command("rm", OPENVPN_PATH+"server.conf").Output()
	exec.Command("rm", OPENVPN_PATH+"ca.crt").Output()
	exec.Command("rm", OPENVPN_PATH+"ta.key").Output()
	exec.Command("rm", OPENVPN_PATH+"server.crt").Output()
	exec.Command("rm", OPENVPN_PATH+"server.key").Output()
	exec.Command("rm", OPENVPN_PATH+"dh2048.pem").Output()
	

	exec.Command("rm","-R" ,OPENVPN_PATH+"/jail/ccd").Output()
	exec.Command("rm", OPENVPN_PATH+"/jail/crl.pem").Output()
	
	

	// Recupération des nouveaux certificats et fichiers de configuaration
	fmt.Println(" * Recupération des nouveaux certificats et fichiers de configuaration")

	_,err=exec.Command("cp", SINASS_DATA_PATH+"openvpn/serverConf/server.conf",OPENVPN_PATH).Output()
	check(err)
	_,err=exec.Command("cp", SINASS_DATA_PATH+"openvpn/serverConf/ca.crt",OPENVPN_PATH).Output()
	check(err)
	_,err=exec.Command("cp", SINASS_DATA_PATH+"openvpn/serverConf/ta.key",OPENVPN_PATH).Output()
	check(err)
	_,err=exec.Command("cp", SINASS_DATA_PATH+"openvpn/serverConf/server.crt",OPENVPN_PATH).Output()
	check(err)
	_,err=exec.Command("cp", SINASS_DATA_PATH+"openvpn/serverConf/server.key",OPENVPN_PATH).Output()
	check(err)
	_,err=exec.Command("cp", SINASS_DATA_PATH+"openvpn/serverConf/dh2048.pem",OPENVPN_PATH).Output()
	check(err)
	

	_,err=exec.Command("cp","-R",SINASS_DATA_PATH+"openvpn/clientsConf/ccd",OPENVPN_PATH+"jail/").Output()
	check(err)
	_,err=exec.Command("cp", SINASS_DATA_PATH+"openvpn/clientsConf/crl/crl.pem",OPENVPN_PATH+"jail/").Output()
	check(err)

	//setSetSinassConfParam(SINASS_DATA_PATH+"sinass.conf", "azerty", "clavier")
	
	return " "
}