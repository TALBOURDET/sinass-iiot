<?php

return [
  'url'=> env('SINASS_URL','localhost'),
  'nbUsers'=> env('SINASS_NB_USERS','50'),
  'nbEquipments'=> env('SINASS_NB_EQUIPMENTS','50'),
  'nbSimultConnections'=> env('SINASS_NB_SIMULT_CONNECTIONS','3'),
  'grafana'=> env('SINASS_GRAFANA','3'),
  'nodeRed'=> env('SINASS_NODERED','3')
  ];
?>