<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VpnServer extends Model
{
    protected $fillable = ['id','ip','networkMask','clientToClient','country','province','city','org','mail'];
}
