<?php namespace App;
 
use Illuminate\Database\Eloquent\Model;
 
class Group extends Model
{ 
    protected $table = 'groups';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'right', 'nbUsers', 'nbEquipments', 'nbSimultConnections','nbActualSimultConnections'
    ];
}
