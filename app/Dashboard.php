<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dashboard extends Model
{
  protected $table = 'dashboards';  
  protected $fillable = ['name','idGroup','type','url','iframe','data'];
  
  public function equipment()
    {
        return $this->hasOne('Equipment');
    }
}
