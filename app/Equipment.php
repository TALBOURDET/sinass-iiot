<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    //
    protected $table = 'equipments';


    protected $fillable = ['ip', 'name', 'webSettings','webIhm','description','latitude','longitude','showOnMap','networkMask','caDir','certDir','keyDir','commonName','idGroup'];
}
