<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Dashboard;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
       // checking acces level
        if (Auth::user()->accessLevel==20) {
            $dashboards  = DB::table('dashboards')->select('id', 'name', 'url','type', 'iframe', 'idGroup','idEquipment')->get();
        }
        else {
            $dashboards  = Dashboard::where('idGroup',Auth::user()->idGroup)->get();
        }
            
    	return response()->json($dashboards);
    }
    public function getMenu()
    {
       // checking acces level
        if (Auth::user()->accessLevel==20) {
            $dashboards  = DB::table('dashboards')
              ->select('dashboards.id', 'dashboards.name', 'dashboards.url','dashboards.type', 'dashboards.iframe', 'dashboards.idGroup','equipments.webIhm','equipments.id as equipmentId')->leftJoin('equipments', 'dashboards.idEquipment', '=', 'equipments.id')
              ->orderBy('dashboards.name', 'asc')
              ->get();
        }
        else {
            $dashboards  = DB::table('dashboards')
              ->select('dashboards.id', 'dashboards.name', 'dashboards.url','dashboards.type', 'dashboards.iframe', 'dashboards.idGroup','equipments.webIhm','equipments.id as equipmentId')->leftJoin('equipments', 'dashboards.idEquipment', '=', 'equipments.id')
              ->where('dashboards.idGroup', '=', Auth::user()->idGroup)
              ->orderBy('dashboards.name', 'asc')
              ->get();
            //$dashboards  = Dashboard::where('idGroup',Auth::user()->idGroup)->get();
        }
            
    	return response()->json($dashboards);
    }
    public function show($id)
    {
      $dashboards  = Dashboard::find($id);
        // checking acces level
        if (($dashboards->idGroup==Auth::user()->idGroup) or (Auth::user()->accessLevel==20)) {
            return response()->json($dashboards);
        } else {
            return response('Forbidden',403);
        }
    }
    
    public function store(Request $request)
    {
      // checking acces level
        if (Auth::user()->accessLevel==20){
            $idGroup = $request->input('idGroup');
        } elseif ((Auth::user()->accessLevel==15) and ($request->input('type')!='dashboard')) {
            $idGroup = Auth::user()->idGroup;
        } else {
            return response('Forbidden',403);
        }
      
        $dashboards = new Dashboard;
        $dashboards->idGroup=$idGroup;
        $dashboards->data ="{}";
        $dashboards->name = $request->input('name');
        $dashboards->idEquipment = $request->input('idEquipment');
        $dashboards->type = $request->input('type');
        $dashboards->url = $request->input('url');
        $dashboards->iframe = $request->input('iframe');
        $dashboards->save();
    }
    public function destroy(Request $request,$id)
    {
      $dashboards  = Dashboard::find($id);
      // checking acces level
      if (($dashboards->idGroup==Auth::user()->idGroup) or (Auth::user()->accessLevel==20)) {
        $dashboards->delete();
        return response('OK',200);
      } else {
        return response('Forbidden',403);
      }
    }
    public function update(Request $request,$id)
    {
      // checking acces level
      if (Auth::user()->accessLevel==20){
          $idGroup = $request->input('idGroup');
      } elseif ((Auth::user()->accessLevel==15) and ($request->input('type')!='dashboard')) {
          $idGroup = Auth::user()->idGroup;
      } else {
          return response('Forbidden',403);
      }
      $dashboards = Dashboard::find($id);
      $dashboards->id = $id;
      if ($request->input('data')!=null) {
        $dashboards->data = $request->input('data');
      }
      $dashboards->name = $request->input('name');
      $dashboards->type = $request->input('type');
      $dashboards->url = $request->input('url');
      $dashboards->idGroup = $request->input('idGroup');
      $dashboards->idEquipment = $request->input('idEquipment');
      $dashboards->iframe = $request->input('iframe');
      $dashboards->save();
      return response('OK',200);
    }
    
    public function delete()
    {
    }
}
