<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class ServicesController extends Controller
{
    public function getStates()
    {
      
      $states["openVpn"]=exec ('sudo OpenVpn -sState');
      if (config('sinass.nodeRed') == 1) {
        $states["nodeRed"]=exec ('sudo SinassServices -status node-red');
      } else  {
        $states["nodeRed"]='Forbidden';
      }
      if (config('sinass.grafana') == 1) {
        $states["grafana"]=exec ('sudo SinassServices -status grafana-server');
      } else {
        $states["grafana"]='Forbidden';
      }
      
      $states["SectorisationVpn"]=exec ('sudo SinassServices -status SectorisationVpn');
 
       
    	return response()->json($states);
    }
  
    public function startService(Request $request)
    {
      if (($request->input('service') == 'nodeRed') and (config('sinass.nodeRed') == 1)) {
        exec ('sudo SinassServices -start node-red');
        //sleep (1);
        return $this->getStates();
      }
      if ($request->input('service') == 'openVpn') {
        exec ('sudo SinassServices -start openvpn');
        //sleep (1);
        return $this->getStates();
      }
      if (($request->input('service') == 'grafana') and (config('sinass.grafana') == 1)) {
        exec ('sudo SinassServices -start grafana-server');
        //sleep (1);
        return $this->getStates();
      }
      if ($request->input('service') == 'SectorisationVpn') {
				exec ('sudo OpenVpn -cTc false');
        exec ('sudo SinassServices -start SectorisationVpn');
        //sleep (1);
        return $this->getStates();
      }
    }
  
    public function stopService(Request $request)
    {
      if (($request->input('service') == 'nodeRed') and (config('sinass.nodeRed') == 1)) {
        exec ('sudo SinassServices -stop node-red');
        //sleep (1);
        return $this->getStates();
      }
      if ($request->input('service') == 'openVpn') {
        exec ('sudo SinassServices -stop openvpn');
        //sleep (1);
        return $this->getStates();
      }
      if (($request->input('service') == 'grafana') and (config('sinass.grafana') == 1)){
        exec ('sudo SinassServices -stop grafana-server');
        //sleep (1);
        return $this->getStates();
      }
      if ($request->input('service') == 'SectorisationVpn') {
				exec ('sudo OpenVpn -cTc true');
        exec ('sudo SinassServices -stop SectorisationVpn');
        //sleep (1);
        return $this->getStates();
      }
    }
}
