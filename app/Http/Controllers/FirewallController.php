<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FirewallController extends Controller
{
		private $firewallRules = [];
	
		private $RULES = ARRAY(
			"nodeRed_1880"	 		=> ARRAY(
				"strRule" => "ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:1880",
				"source" 	=> "anywhere",
				"destination" => "anywhere",
				"protocole" => "TCP",
				"port" => "1880"
			),
			"grafana_3000"	 		=> ARRAY(
				"strRule" => "ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:3000",
				"source" 	=> "anywhere",
				"destination" => "anywhere",
				"protocole" => "TCP",
				"port" => "3000"
			),
			"ftps_21"	 		=> ARRAY(
				"strRule" => "ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:ftp",
				"source" 	=> "anywhere",
				"destination" => "anywhere",
				"protocole" => "TCP",
				"port" => "21"
			),
			"openVpn_1194_TCP"	 		=> ARRAY(
				"strRule" => "ACCEPT     tcp  --  anywhere             anywhere             tcp dpt:openvpn",
				"source" 	=> "anywhere",
				"destination" => "anywhere",
				"protocole" => "TCP",
				"port" => "1194"
			),
			"openVpn_1194_UDP"	 		=> ARRAY(
				"strRule" => "ACCEPT     udp  --  anywhere             anywhere             udp dpt:openvpn",
				"source" 	=> "anywhere",
				"destination" => "anywhere",
				"protocole" => "UDP",
				"port" => "1194"
			)
		);

	
		/**
 		* Retourne l'état des règle du firewall
 		*
 		* @api
		* @return JSON liste des règles et leurs états
 		*/
    public function getRules()
    {
			$this->getServerRules();
				
			$result = (object)[];
			$result->nodeRed_1880 = $this->rechercheRule($this->firewallRules,$this->RULES["nodeRed_1880"]["strRule"])->state;
			$result->grafana_3000 = $this->rechercheRule($this->firewallRules,$this->RULES["grafana_3000"]["strRule"])->state;
			$result->ftps_21 = $this->rechercheRule($this->firewallRules,$this->RULES["ftps_21"]["strRule"])->state;
			$result->openVpn_1194_TCP = $this->rechercheRule($this->firewallRules,$this->RULES["openVpn_1194_TCP"]["strRule"])->state;
			$result->openVpn_1194_UDP = $this->rechercheRule($this->firewallRules,$this->RULES["openVpn_1194_UDP"]["strRule"])->state;
			
			return response()->json($result);
    }
		/**
 		* Modifie les règles du firwall
 		*
 		* @api
		* @param JSON $request
		* @return HTTP RESPONSE 
 		*/
		public function setRules(Request $request)
		{
			$this->setRule($request, "nodeRed_1880");
			$this->setRule($request, "grafana_3000");
			$this->setRule($request, "ftps_21");
			$this->setRule($request, "openVpn_1194_TCP");
			$this->setRule($request, "openVpn_1194_UDP");
			
			return response('ok',200);
		
			
		}
		/**
 		* Insert ou supprime une règles du firwall
 		*
		* @param JSON $request
		* @param STRING $ruleName
		* @return BOOLEAN true if ok
 		*/
		private function setRule(Request $request, $ruleName)
		{
			$this->getServerRules();
			if (($this->rechercheRule($this->firewallRules,$this->RULES[$ruleName]["strRule"])->state == true) and ($request->{$ruleName} == false)) {
				exec('sudo SinassServices -firewall -removeRule '.$this->rechercheRule($this->firewallRules,$this->RULES[$ruleName]["strRule"])->num);
				return true;
			} elseif (($this->rechercheRule($this->firewallRules,$this->RULES[$ruleName]["strRule"])->state == false) and ($request->{$ruleName} == true)) {
				exec('sudo SinassServices -firewall -insertRule '.$this->RULES[$ruleName]["protocole"].' '.$this->RULES[$ruleName]["port"]);
				return true;
			} else {
				return true;
			}
		}
		/**
 		* Actualise $firewallRules avec toutes les règle du firewall
 		*
		* @return ARRAY liste des règles du firewall
 		*/
		private function getServerRules()
		{
			exec ('sudo SinassServices -firewall -getRules',$outExec);
			unset($outExec[0]);
			unset($outExec[1]);
			
			$end = false;
			$i = 1;
			foreach($outExec as $line) {
				if ($line <> "" and $end == false) {
					$line=substr_replace($line,"",0,5);	
					$RulesList[$i] = $line;
					$i = $i+1;
				} else {
					$end = true;
				}
			}
			
			$this->firewallRules = $RulesList;
			return $this->firewallRules;
		}
		/**
 		* Recherche si une règle est présente dans le firewall
 		*
		* @param (object)[] $rulesList liste des règle dans laquelle effectuer la recherche
		* @param STRING $search règle recherché
		* @return BOOLEAN
 		*/
		private function rechercheRule($rulesList,$search)
		{
			$i = 1;
			foreach($rulesList as $rule) 
			{	
				$result = (object)[];
				
				if ($rule==$search) {
					$result->state = true;
					$result->num = $i;
					return $result;
				}
				$i = $i + 1;
			}
			$result->state = false;
			return $result;
		}
}
