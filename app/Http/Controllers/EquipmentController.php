<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Equipment;

class EquipmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function index()
    {
        // checking acces level
        if (Auth::user()->accessLevel==20) {
            $equipments  = Equipment::all();
        }
        else {
            $equipments  = Equipment::where('idGroup',Auth::user()->idGroup)->get();
        }
      //return config('sinass.url');
    	return response()->json($equipments);
    }
    public function getStates()
    {
        exec ('sudo OpenVpn -cStates',$outExec);
        // checking acces level
        if (Auth::user()->accessLevel==20) {
            $equipments  = Equipment::all();
        }
        else {
            $equipments  = Equipment::where('idGroup','=',Auth::user()->idGroup)->get();
        }
        $return=[];

        foreach ($equipments as $equipment){
            for ($j = 2; $j <= count($outExec); $j++) {
                if ($outExec[$j]=="ROUTING TABLE") {
                    break;
                } else {
                    if (explode (",",$outExec[$j])[0]==$equipment->commonName){
                        $equipment->connected=true;
                        break;
                    } else{
                        $equipment->connected=false;
                }
            }

        }
        array_push($return,$equipment);
        }

    	return response()->json($return);
    }
    public function show($id)
    {
    	$equipments  = Equipment::find($id);
        // checking acces level
        if (($equipments->idGroup==Auth::user()->idGroup) or (Auth::user()->accessLevel==20)) {
            return response()->json($equipments);
        } else {
            return response('Forbidden',403);
        }
    }
    private function testMaxEquipment($idGroup)
    {
      $nbGlobalEquipments = DB::table('equipments')
        ->select(DB::raw('count(*) as nb'))
        ->get();
      $nbEquipments = DB::table('equipments')
        ->select(DB::raw('count(*) as nb'))
        ->where('idGroup', '=', $idGroup)
        ->get();
      
      $nbMaxEquipments = DB::table('groups')
        ->select(DB::raw('nbEquipments'))
        ->where('id', '=', $idGroup)
        ->get();
      if (($nbEquipments[0]->nb < $nbMaxEquipments[0]->nbEquipments) and ($nbGlobalEquipments[0]->nb < config('sinass.nbEquipments'))) {
        return true;
      } else  {
        return false;
      }
      
      
    }
    public function store(Request $request)
    {
        
        // checking acces level
        if (Auth::user()->accessLevel==20){
            $idGroup = $request->input('idGroup');
        } elseif (Auth::user()->accessLevel==15) {
            $idGroup = Auth::user()->idGroup;
        } else {
            return response('Forbidden',403);
        }
        if ($this->testMaxEquipment($idGroup)==true) {
          if ($request->input('setConfVpn')) {
            $commonName = str_replace(' ', '_', $request->input('name'));
            $commonName = $commonName . '_' . strval(rand (0,1000000000));

            $equipment = new Equipment;
            $equipment->name = $request->input('name');
            $equipment->ip = $request->input('ip');
            $equipment->webSettings = $request->input('webSettings');
            $equipment->webIhm = $request->input('webIhm');
            $equipment->description = $request->input('description');
            $equipment->latitude = $request->input('latitude');
            $equipment->longitude = $request->input('longitude');
            $equipment->showOnMap = $request->input('showOnMap');
            $equipment->networkMask = $request->input('networkMask');
            //$equipment->caDir = $request->input('caDir');
            //$equipment->certDir = $request->input('certDir');
            //$equipment->keyDir = $request->input('keyDir');
            $equipment->commonName = $commonName;
            //$equipment->openVpnEnabled = true;
            $equipment->idGroup = $idGroup;
            $equipment->save();

            $returnExec = exec ('sudo OpenVpn -c ' . $commonName . ' '.config('sinass.url').' ' . $request->input('ip') . ' ' . $request->input('networkMask') . ' ' . $request->input('caDir') . ' ' . $request->input('certDir') . ' ' . $request->input('keyDir'));
            if ($returnExec == 'done') {
                return response()->json($equipment);
            } else {
                return 'Problème création certificat client';
            }

          } else {
            //$equipment = Equipment::create($request->all());
            $equipment = new Equipment;
            $equipment->name = $request->input('name');
            $equipment->ip = $request->input('ip');
            $equipment->webSettings = $request->input('webSettings');
            $equipment->webIhm = $request->input('webIhm');
            $equipment->description = $request->input('description');
            $equipment->latitude = $request->input('latitude');
            $equipment->longitude = $request->input('longitude');
            $equipment->showOnMap = $request->input('showOnMap');
            $equipment->networkMask = $request->input('networkMask');
            //$equipment->caDir = $request->input('caDir');
            //$equipment->certDir = $request->input('certDir');
            //$equipment->keyDir = $request->input('keyDir');
            //$equipment->commonName = $commonName;
            //$equipment->openVpnEnabled = false;
            $equipment->idGroup = $idGroup;
            $equipment->save();

            return response()->json($equipment);
          }
        } else {
            return response('Forbidden (nb max Equipments)',403);
        }
        
    }

    public function update(Request $request,$id)
    {
        // checking acces level
        if (Auth::user()->accessLevel==20){
            $idGroup = $request->input('idGroup');
        } elseif (Auth::user()->accessLevel==15) {
            $idGroup = Auth::user()->idGroup;
        } else {
            return response('Forbidden',403);
        }
        $equipment = Equipment::find($id);
        if (($equipment->commonName != null) and (($request->input('ip') != $equipment->ip) or ($equipment->networkMask != $request->input('networkMask')))){
            $returnExec = exec ('sudo OpenVpn -cUIp ' . $equipment->commonName . ' ' . $request->input('ip'). ' ' .$request->input('networkMask'));
        }
    	$equipment->name = $request->input('name');
    	$equipment->ip = $request->input('ip');
    	$equipment->webSettings = $request->input('webSettings');
        $equipment->webIhm = $request->input('webIhm');
        $equipment->description = $request->input('description');
        $equipment->latitude = $request->input('latitude');
        $equipment->longitude = $request->input('longitude');
        $equipment->showOnMap = $request->input('showOnMap');
        $equipment->networkMask = $request->input('networkMask');
        $equipment->caDir = $request->input('caDir');
        $equipment->certDir = $request->input('certDir');
        $equipment->keyDir = $request->input('keyDir');
        $equipment->idGroup = $idGroup;


        if (isset($returnExec)){
            if ($returnExec == 'done') {
                $equipment->save();
                return response()->json($equipment);
            } else {
                return response('Erreur lors de la revoquation',500);
            }
        } else {
            $equipment->save();
        }
    }
    public function revoke($id)
    {
        $equipment = Equipment::find($id);

        // checking acces level
        if (($equipment->idGroup == Auth::user()->idGroup and Auth::user()->accessLevel==15) or (Auth::user()->accessLevel==20)){
            $returnExec = exec ('sudo OpenVpn -r ' . $equipment->commonName);
            $equipment->commonName = null;
            $equipment->openVpnEnabled = false;
            if ($returnExec == 'done') {
                $equipment->save();
                return response()->json($equipment);
            } else {
                return response('Erreur lors de la revoquation',500);
            }
        } else {
            return response('Forbidden',403);
        }

    }
    public function setConfFileCert($id)
    {
        $equipment = Equipment::find($id);

        // checking acces level
        if (($equipment->idGroup == Auth::user()->idGroup and Auth::user()->accessLevel==15) or (Auth::user()->accessLevel==20)){
            $commonName = str_replace(' ', '_', $equipment->name);
            $commonName = $commonName . '_' . strval(rand (0,1000000000));
            $equipment->commonName = $commonName;
            $equipment->openVpnEnabled = true;
            $returnExec = exec ('sudo OpenVpn -c ' . $equipment->commonName .' '.config('sinass.url') .' '. $equipment->ip . ' ' . $equipment->networkMask . ' ' . $equipment->caDir . ' ' . $equipment->certDir . ' ' . $equipment->keyDir);

            if ($returnExec == 'done') {
                $equipment->save();
                return response()->json($equipment);
            } else {
                return response('Erreur lors de la création du certificat',500);
            }
        } else {
            return response('Forbidden',403);
        }
    }
    public function destroy($id)
    {
        $equipment = Equipment::find($id);
        //return response()->json($equipment->idGroup);
        // checking acces level
        if (($equipment->idGroup == Auth::user()->idGroup and Auth::user()->accessLevel==15) or (Auth::user()->accessLevel==20)){
            if ($equipment->commonName != null) {
                $returnExec = exec ('sudo OpenVpn -r ' . $equipment->commonName);
            }
            $equipment->delete();
            return response('',200);
        } else {
            return response('Forbidden',403);
        }
    }
    public function getConfFile($commonName)
    {
        $equipment = Equipment::where('commonName',$commonName)->first();

        // checking acces level
        if (($equipment->idGroup == Auth::user()->idGroup and Auth::user()->accessLevel==15) or (Auth::user()->accessLevel==20)){
            return response()->download("/home/ubuntu/Sinass-datas/openvpn/clientsConf/".$commonName."/".$commonName.".ovpn");
        } else {
            return response('Forbidden',403);
        }
    }
}
