<?php

namespace App\Http\Controllers;

use App\Group;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
    	$groups  = Group::all();
 
    	return response()->json($groups);
    }
    
    public function show($id)
    {
    	$groups  = Group::find($id);
 
    	return response()->json($groups);
    }
    
    public function store(Request $request)
    {
        $group = Group::create($request->all());
 
    	return response()->json($group);    
    }
    
    public function update(Request $request,$id)
    {
        $Group = Group::find($id);
    	  $Group->name = $request->input('name');
    	  $Group->right = $request->input('right');
        $Group->nbUsers = $request->input('nbUsers');
        $Group->nbEquipments = $request->input('nbEquipments');
        $Group->nbSimultConnections = $request->input('nbSimultConnections');
    	  $Group->save();
        
    	return response()->json($Group);    
    }
    
    public function destroy($id)
    {  
        $group = Group::find($id);

        $group->delete();
        
        return response()->json($group);
    }
}

