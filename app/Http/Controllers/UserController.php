<?php

namespace App\Http\Controllers;

use App\User;
use App\VpnClient;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Mail\passwordLost;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        // checking acces level
        if (Auth::user()->accessLevel==20) {
            $users  = User::all();
        } else {
            $users  = User::where('idGroup',Auth::user()->idGroup)->get();
        }
    	return response()->json($users);
    }
    public function passwordLost(Request $request)
    {
        $user = User::where('email','=',$request->input('email'))->get();
        //return response()->json($user);
        $user = User::find($user[0]->id);

        $char = 'abcdefghijklmnopqrstuvwxyz0123456789';
        $mot_de_passe = str_shuffle($char);
        $mot_de_passe = substr($mot_de_passe,0,5);

        $user->password = bcrypt($mot_de_passe);

        $user->save();
        $user->passwordMail = $mot_de_passe;
        Mail::to($user->email)
            ->send(new passwordLost($user));
        return response()->json($request->input('email'));
    }
    public function getVpn($user_id)
    {
        $user = User::find($user_id);
        $vpn = User::find($user_id)->vpnClients;
        // checking acces level
        if (Auth::user()->accessLevel==20) {
            return response()->json($vpn);
        } elseif ((Auth::user()->accessLevel==15) and (Auth::user()->idGroup == $user->idGroup)) {
            return response()->json($vpn);
        } elseif (Auth::user()->id == $user->id){
            return response()->json($vpn);
        }else {
            return response('Forbidden',403);
        }

    }
    public function show($id)
    {
    	$user  = User::find($id);
        // checking acces level
        if (Auth::user()->accessLevel==20) {
            return response()->json($user);
        } if ((Auth::user()->accessLevel==15) and (Auth::user()->idGroup == $user->idGroup)) {
            return response()->json($user);
        } else {
            return response('Forbidden',403);
        }

    }
    public function getAccount()
    {
    	$user  = User::find(Auth::user()->id);
        return response()->json($user);
    }
     private function testMaxUser($idGroup)
    {
      $nbGlobalUsers = DB::table('users')
        ->select(DB::raw('count(*) as nb'))
        ->get();
      
      $nbUsers = DB::table('users')
        ->select(DB::raw('count(*) as nb'))
        ->where('idGroup', '=', $idGroup)
        ->get();
      
      $nbMaxUsers = DB::table('groups')
        ->select(DB::raw('nbUsers'))
        ->where('id', '=', $idGroup)
        ->get();
      if (($nbUsers[0]->nb < $nbMaxUsers[0]->nbUsers) and ($nbGlobalUsers[0]->nb < config('sinass.nbUsers'))) {
        return true;
      } else  {
        return false;
      }    
    }
    public function store(Request $request)
    {
        
        // checking acces level
        if (Auth::user()->accessLevel==20) {
            if ($this->testMaxUser($request->input('idGroup'))==true) {
              $user = User::create([
                  'email'=>$request->input('email'),
                  'firstName'=>$request->input('firstName'),
                  'lastName'=>$request->input('lastName'),
                  'login'=>$request->input('login'),
                  'idGroup'=>$request->input('idGroup'),
                  'password'=>bcrypt($request->input('password')),
                  'accessLevel'=>$request->input('accessLevel'),
              ]);
              return response()->json($user);
            } else {
              return response('Forbidden (nb max Users)',403);
            }
        }  elseif ((Auth::user()->accessLevel==15) and ($request->input('accessLevel') <= Auth::user()->accessLevel)) {
            if ($this->testMaxUser(Auth::user()->idGroup)==true) {
              $user = User::create([
                  'email'=>$request->input('email'),
                  'firstName'=>$request->input('firstName'),
                  'lastName'=>$request->input('lastName'),
                  'login'=>$request->input('login'),
                  'idGroup'=>Auth::user()->idGroup,
                  'password'=>bcrypt($request->input('password')),
                  'accessLevel'=>$request->input('accessLevel'),
              ]);
              return response()->json($user);
            } else {
              return response('Forbidden (nb max Users)',403);  
            }
        } else {
            return response('Forbidden',403);
        }
    }

    public function update(Request $request,$id)
    {
        // checking acces level
        if (Auth::user()->accessLevel==20) {
          $User = User::find($id);
        	$User->email = $request->input('email');
        	$User->firstName = $request->input('firstName');
        	$User->lastName = $request->input('lastName');
            $User->login = $request->input('login');
            $User->idGroup = $request->input('idGroup');
            $User->accessLevel = $request->input('accessLevel');
            if ($request->input('password')!='') {
                $User->password = bcrypt($request->input('password'));
            }
            $User->right = $request->input('right');
        	$User->save();

            return response('OK',200);

        }  elseif ((Auth::user()->accessLevel==15) and ($request->input('idGroup')==Auth::user()->idGroup) and ($request->input('accessLevel') <= Auth::user()->accessLevel)) {
            $User = User::find($id);
        	$User->email = $request->input('email');
        	$User->firstName = $request->input('firstName');
        	$User->lastName = $request->input('lastName');
            $User->login = $request->input('login');
            $User->idGroup = $request->input('idGroup');
            $User->accessLevel = $request->input('accessLevel');
            if ($request->input('password')!='') {
                $User->password = bcrypt($request->input('password'));
            }
            $User->right = $request->input('right');
        	$User->save();

            return response('OK',200);
        } else {
            return response('Forbidden',403);
        }

    }

    public function updateAccount(Request $request)
    {
        $User = User::find(Auth::user()->id);
    	$User->email = $request->input('email');
    	$User->firstName = $request->input('firstName');
    	$User->lastName = $request->input('lastName');
        $User->login = $request->input('login');
        if ($request->input('password')!='') {
            $User->password = bcrypt($request->input('password'));
        }
    	$User->save();

    	return response()->json($User);
    }
    public function destroy($id)
    {
        $user = User::find($id);

        // checking acces level
        if ((Auth::user()->accessLevel==20) or ((Auth::user()->accessLevel==15) and ($user->idGroup==Auth::user()->idGroup) and ($user->accessLevel <= Auth::user()->accessLevel))) {
            $vpnClients = VpnClient::where('user_id', '=', $id)->get();
            foreach ($vpnClients as $vpnClient) {
                $this->revoke($vpnClient->commonName);
            }
            $user->delete();
            return response('OK',200);
        }  else {
            return response('Forbidden',403);
        }

    }
    public function setConfFileCert(Request $request, $id)
    {
        $user = User::find($id);
        // checking acces level
        if ((Auth::user()->accessLevel==20) or ((Auth::user()->accessLevel==15) and ($user->idGroup==Auth::user()->idGroup) and ($user->accessLevel <= Auth::user()->accessLevel))){  
						$commonName = $user->login . '_' . $request->input('name') . '-' . strval(rand (0,1000000000));
						$commonName = str_replace(' ', '_', $commonName);
					
            $returnExec = exec ('sudo OpenVpn -c ' . $commonName . ' '.config('sinass.url'));
            if ($returnExec == 'done') {
                $vpnClient = VpnClient::create([
                    'commonName'=>$commonName,
                    'user_id'=>$id,
            ]);
                return response()->json($vpnClient);
            } else {
                return response('Internal Server Error',500);
            }
        } else {
            return response('Forbidden',403);
        }
    }
    public function revoke($commonName)
    {
        $vpnClient = VpnClient::where('commonName', '=', $commonName)->first();
        $user = User::find($vpnClient->user_id);
        // checking acces level
        if ((Auth::user()->accessLevel==20) or ((Auth::user()->accessLevel==15) and ($user->idGroup==Auth::user()->idGroup)and ($user->accessLevel <= Auth::user()->accessLevel))) {
            $returnExec = exec ('sudo OpenVpn -r ' . $commonName);

            if ($returnExec == 'done') {
                $vpnClient = VpnClient::where('commonName', '=', $commonName);
                $vpnClient->delete();
                return "done";
            } else {
                return response('Internal Server Error',500);
            }
            return response('OK',200);
        } else {
            return response('Forbidden',403);
        }
    }
    public function getConfFile($commonName)
    {
        $vpnClient = VpnClient::where('commonName', '=', $commonName)->first();
        $user = User::find($vpnClient->user_id);
        // checking acces level
        if ((Auth::user()->accessLevel==20) or ((Auth::user()->accessLevel==15) and ($user->idGroup==Auth::user()->idGroup) and ($user->accessLevel <= Auth::user()->accessLevel)) or ($user->id==Auth::user()->id)) {
            return response()->download("/home/ubuntu/Sinass-datas/openvpn/clientsConf/".$commonName."/".$commonName.".ovpn");
        } else {
            return response('Forbidden',403);
        }
    }
}
