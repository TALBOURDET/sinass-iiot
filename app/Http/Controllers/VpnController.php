<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\VpnServer;

class VpnController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function getLogs()
    {
    	exec ('sudo OpenVpn -logs',$returnExec); 
    	return response()->json($returnExec);
    }
    public function getStates()
    {
    	exec ('sudo OpenVpn -cStates',$outExec);
        $return = [];
        $iReturn = 1;
        for ($i = 3; $i <= count($outExec); $i++) {
             if ($outExec[$i]=="ROUTING TABLE") {
                 break;
            } else {
                $return[$iReturn]=$outExec[$i];
                $iReturn++;
             } 
                 
        }
    	return response()->json($return);
    }
    public function getState()
    {
    	$outExec=exec ('sudo OpenVpn -sState');
       
    	return response()->json($outExec);
    }
    public function startServer()
    {
        exec ('sudo OpenVpn -start');
    	$outExec=exec ('sudo OpenVpn -sState');
       
    	return response()->json($outExec);
    }
    public function stopServer()
    {
        exec ('sudo OpenVpn -stop');
    	$outExec=exec ('sudo OpenVpn -sState');
       
    	return response()->json($outExec);
    }
    public function getOvpnConf()
    {
        $vpnServer  = VpnServer::find(1);
 
    	return response()->json($vpnServer);
    }
    public function updateOvpnConf(Request $request,$id)
    {
        $outExec=exec ('sudo OpenVpn -s '. $request->input('ip') .' '. $request->input('networkMask') . ' ' . $request->input('clientToClient') . ' ' . $request->input('country') . ' ' . $request->input('province') . ' ' . $request->input('city') . ' ' . $request->input('org') . ' ' . $request->input('mail'));
      
        $vpnServer  = VpnServer::find($id);
        
        $vpnServer->ip = $request->input('ip');
        $vpnServer->networkMask = $request->input('networkMask');
        $vpnServer->clientToClient = $request->input('clientToClient');
        $vpnServer->country = $request->input('country');
        $vpnServer->province = $request->input('province');
        $vpnServer->city = $request->input('city');
        $vpnServer->org = $request->input('org');
        $vpnServer->mail = $request->input('mail');
        $vpnServer->save();

    	return response()->json($vpnServer);
    }
}
