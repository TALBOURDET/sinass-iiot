<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class accessLevel
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,...$accessLevel)
    {
        if (in_array(Auth::user()->accessLevel,$accessLevel)){
            return $next($request);
        } else {
            return response('Forbidden',403);
        }


    }
}
