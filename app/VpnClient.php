<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VpnClient extends Model
{
    protected $fillable = ['ip','networkMask','caDir','certDir','keyDir','commonName','user_id','equipment_id','id'];
}
