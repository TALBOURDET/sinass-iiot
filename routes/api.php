<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => '/v1'], function($app)
{
    $app->post('passwordLost','UserController@passwordLost');
});
//, 'middleware' => 'auth:api'
Route::group(['prefix' => '/v1', 'middleware' => 'auth:api'], function($app)
{
        $app->get('dashboard/menu','DashboardController@getMenu');
				$app->get('dashboard','DashboardController@index');
				$app->get('dashboard/{id}','DashboardController@show');
				$app->post('dashboard','DashboardController@store')->middleware('accessLevel:15,20');
				$app->put('dashboard/{id}','DashboardController@update')->middleware('accessLevel:15,20');
				$app->delete('dashboard/{id}','DashboardController@destroy')->middleware('accessLevel:15,20');
	
				$app->get('equipment/state','EquipmentController@getStates');
	    	$app->get('equipment','EquipmentController@index');
        $app->get('equipment/{id}','EquipmentController@show');
        $app->post('equipment','EquipmentController@store')->middleware('accessLevel:15,20');
        $app->put('equipment/{id}','EquipmentController@update')->middleware('accessLevel:15,20');
        $app->delete('equipment/{id}','EquipmentController@destroy')->middleware('accessLevel:15,20');
        $app->post('equipment/download/simpleConfFile/{commonName}','EquipmentController@getConfFile')->middleware('accessLevel:15,20');
        $app->post('equipment/revoke/{id}','EquipmentController@revoke')->middleware('accessLevel:15,20');
        $app->post('equipment/create/ConfFileCert/{id}','EquipmentController@setConfFileCert')->middleware('accessLevel:15,20');


        $app->get('user','UserController@index');
        $app->get('account','UserController@getAccount');
        $app->get('user/getVpn/{user_id}','UserController@getVpn');
        $app->get('user/{id}','UserController@show');
        $app->post('user','UserController@store')->middleware('accessLevel:15,20');
        $app->put('account','UserController@updateAccount');
        $app->put('user/{id}','UserController@update')->middleware('accessLevel:15,20');
        $app->delete('user/{id}','UserController@destroy')->middleware('accessLevel:15,20');
        $app->post('user/download/simpleConfFile/{commonName}','UserController@getConfFile');
        $app->delete('user/revoke/{commonName}','UserController@revoke')->middleware('accessLevel:15,20');
        $app->post('user/create/ConfFileCert/{id}','UserController@setConfFileCert')->middleware('accessLevel:15,20');

        $app->get('vpn/logs','VpnController@getLogs')->middleware('accessLevel:15,20');
        $app->get('vpn/clientsStates','VpnController@getStates');
        $app->get('vpn/serverState','VpnController@getState');
        $app->post('vpn/startServer','VpnController@startServer')->middleware('accessLevel:20');
        $app->post('vpn/stopServer','VpnController@stopServer')->middleware('accessLevel:20');

        $app->get('vpn/OvpnConf','VpnController@getOvpnConf');
        $app->put('vpn/OvpnConf/{id}','VpnController@updateOvpnConf')->middleware('accessLevel:20');

				$app->get('services/servicesStates','ServicesController@getStates');
				$app->post('services/startService','ServicesController@startService')->middleware('accessLevel:20');
				$app->post('services/stopService','ServicesController@stopService')->middleware('accessLevel:20');

				
        $app->get('group','GroupController@index')->middleware('accessLevel:20');
        $app->get('group/{id}','GroupController@show')->middleware('accessLevel:20');
        $app->post('group','GroupController@store')->middleware('accessLevel:20');
        $app->put('group/{id}','GroupController@update')->middleware('accessLevel:20');
        $app->delete('group/{id}','GroupController@destroy')->middleware('accessLevel:20');
	
				$app->get('firewall','FirewallController@getRules')->middleware('accessLevel:20');
				$app->put('firewall','FirewallController@setRules')->middleware('accessLevel:20');
	
				
});
